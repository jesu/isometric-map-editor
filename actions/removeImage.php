<?php
// Evaluación
	if( $_POST && $_POST['name'] ){

		$name = $_POST['name'];
// Se eliminar del fichero

		$fichero = '../map/images/'.$name;

		if( !file_exists( $fichero ) ){
			$msg = array(
				'success' => false,
				'error' => 'ERROR al eliminar IMAGEN - El servidor no ha encontrado ninguna con el nombre '.$name
			);
		}else{

			unlink( $fichero );

			$msg = array(
				'success' => true,
				'message' => 'Imagen '.$name.' eliminada correctamente'
			);
		}

	}else{
		$msg = array(
			'success' => false,
			'error' => 'ERROR al eliminar IMAGEN - Tiene que haber un nombre :('
		);
	}	

	echo json_encode( $msg );
?>