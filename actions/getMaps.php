<?php
		// Get List of images from folder
			$dir = '../map/maps';
			$mapList = array();
			$filesList = [];

			if ( is_dir( $dir ) ) {
			    if ( $dh = opendir( $dir ) ) {
			        while ( ( $file = readdir( $dh ) ) !== false ) {
			        	if( strlen( $file ) > 4 ){
			            	array_push( $filesList, $file );
			            }
			        }
			        closedir( $dh );
			    }
			}

			for ($i=0; $i < count($filesList); $i++) {
				$fichero = $dir.'/'.$filesList[ $i ];
				if ( file_exists( $fichero ) ){
					$mapJSONData = json_decode( file_get_contents( $fichero ), true );
					$mapNeedData = array(
						"name" => $mapJSONData[ "name" ],
						"type" => $mapJSONData[ "type" ],
						"size" => $mapJSONData[ "size" ],
						"create" => $mapJSONData[ "create" ]
					);
					if( !empty($mapJSONData[ "parent" ]) ){
						$mapNeedData[ "parent" ] = $mapJSONData[ "parent" ];
					}

					array_push( $mapList, $mapNeedData );
				}
			}

			$response = array(
				'maps' => $mapList
			);

		echo json_encode( $response );
?>