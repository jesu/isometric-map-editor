<?php
// Take all the shit!
	$name = $_POST['name'];
	$type = $_POST['type'];
	$race = $_POST['race'];
	$animationSpeed = $_POST['animationSpeed'];
	$src = $_POST['src'];
	$alpha = $_POST['alpha'];
	$blendMode = $_POST['blendMode'];
	$layer = $_POST['layer'];
	$order = $_POST['order'];
	$scalex = $_POST['scalex'];
	$scaley = $_POST['scaley'];
	$x = $_POST['x'];
	$y = $_POST['y'];
	$subElements = $_POST['subElements'];
	$Canvas = $_POST['Canvas'];
	$WebGL = $_POST['WebGL'];

// Evaluación
	if( $name && $name != 'false' 
		&& $src && $src != 'false' 
		&& $type && $type != 'false' 
		&& $race && $race != 'false' ){

		$srcList = explode(",", $src);
		if( count($srcList) > 1 ){
			$srcValue = $srcList;
		}else{
			$srcValue = $srcList[0];
		}

		$element = array(
			'type' => $type,
			'race' => $race,
			'src' => $srcValue
		);

		if( $type === "movieClip" || $type === "movieClipLight" ){
			$element[ 'animationSpeed' ] = $animationSpeed;
		}

		if( $x == 'false' ){
			$x = 0;
		}
		if( $y == 'false' ){
			$y = 0;
		}
		if( $layer == 'false' ){
			$layer = 'ground';
		}
		$element[ 'x' ] = $x;
		$element[ 'y' ] = $y;
		$element[ 'layer' ] = $layer;

		if( $order != 'false' ){
			$element[ 'order' ] = $order;
		}
		if( $alpha != 'false' && $alpha != '1' ){
			$element[ 'alpha' ] = $alpha;
		}
		if( $blendMode != 'false' && $blendMode != 'NORMAL' ){
			$element[ 'blendMode' ] = $blendMode;
		}
		if( $scalex != 'false' || $scaley != 'false' ){
			$scale = array(
				'x' => 1,
				'y' => 1
			);
			if( $scalex == 'true' ){
				$scale[ 'x' ] = -1;
			}
			if( $scaley == 'true' ){
				$scale[ 'y' ] = -1;
			}
			$element[ 'scale' ] = $scale;
		}
		if( $subElements != 'false' ){
			$element[ 'subElements' ] = explode( ",", $subElements );
		}
		if( $Canvas != 'false' ){
			$element[ 'Canvas' ] = $Canvas;
		}
		if( $WebGL != 'false' ){
			$element[ 'WebGL' ] = $WebGL;
		}

		//echo json_encode($element);

// Se añade en el fichero

		$fichero = '../map/mapElements.json';
		$mapElObj = json_decode( file_get_contents( $fichero ), true );

		if( !empty( $mapElObj['elements'][ $name ] ) ){
			$msg = array(
				'success' => false,
				'error' => 'ERROR al crear NUEVO ELEMENTO - Ya existe un elemento con el nombre '.$name
			);
		}else{

			$mapElObj['elements'][ $name ] = $element;

			file_put_contents( $fichero, json_encode( $mapElObj ) );

			$msg = array(
				'success' => true,
				'message' => 'Nuevo elemento '.$name.' creado correctamente'
			);
		}

	}else{
		$msg = array(
			'success' => false,
			'error' => 'ERROR al crear NUEVO ELEMENTO - Tiene que haber un nombre e imagen :('
		);
	}	

	echo json_encode( $msg );
?>