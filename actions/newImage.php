<?php

$name = $_POST['name'];

$error = '';
$imagesPath = '../map/images/';

// Check for errors
if($_FILES['image']['error'] > 0){
    $error = 'Parece que ha habido un error mientras se subia la imagen';
}

if( !getimagesize( $_FILES['image']['tmp_name'] ) ){
    $error = 'Por favor, asegúrate que estas subiendo una imagen';
}
/*
// Check filetype
if($_FILES['image']['type'] != 'image/png'){
    $error = 'Unsupported filetype uploaded.';
}
*/
// Check filesize
if( $_FILES['image']['size'] > 500000 ){
    $error = 'El fichero excede el tamaño, prueba con algo más pequeño o pregunta qué hacer a tu programador de confianza.';
}

$newName = $_FILES['image']['name'];
if( !empty( $name ) ){
	// Preparamos el nuevo nombre
	$nameParts = explode(".", $newName );
	$newName = $name . '.' . end($nameParts);
}

// Check if the file exists
if( file_exists( $imagesPath . $newName ) ){
    $error = '¡Ya existe un fichero con ese nombre!';
}




// Upload file
if( !move_uploaded_file( $_FILES['image']['tmp_name'], $imagesPath . $newName ) ){
    $error = 'Error cargado el fichero, parece que no hay permisos de escritura';
}

// Success!
if( empty($error) ){
	$msg = array(
		'success' => true,
		'message' => '¡La imagen se ha añadido a la colección correctamente!'
	);
}else{
	$msg = array(
		'success' => false,
		'error' => $error
	);
}

echo json_encode( $msg );

?>