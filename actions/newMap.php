<?php
// Take all the shit!
	$name = $_POST['name'];
	$size = $_POST['size'];
	$type = $_POST['type'];
	$parent = $_POST['parent'];

// Evaluación
	if( $name && $name != 'false' 
		&& $size && $size != 'false'
		&& $type && $type != 'false'){

		$map = array(
			'name' => $name
		);

		$map[ 'size' ] = $size;
		$map[ 'type' ] = $type;
		$map[ 'create' ] = date("Y-m-d H:i:s");
		$map[ 'content' ] = array();

		$dimCells = 2 * $size + 1;

		for ( $r=0; $r < $dimCells; $r++ ) { 
			$row = [];
			for ( $c=0; $c < $dimCells; $c++ ) { 
				array_push( $row, [] );
			}
			array_push( $map[ 'content' ], $row );
		}

		if( $type && $type != 'false' ){
			$map[ 'parent' ] = $parent;
		}

// Se añade en el fichero

		$fichero = '../map/maps/'.$name.'.json';

		if( file_exists( $fichero ) ){
			$msg = array(
				'success' => false,
				'error' => 'ERROR al crear NUEVO MAPA - Ya existe un mapa con el nombre '.$name
			);
		}else{

			$fp = fopen( $fichero, "wb" );
			fwrite( $fp, json_encode( $map ) );
			fclose( $fp );

			$msg = array(
				'success' => true,
				'message' => 'Nuevo mapa '.$name.' creado correctamente'
			);
		}

	}else{
		$msg = array(
			'success' => false,
			'error' => 'ERROR al crear NUEVO MAPA - Todos los campos son obligatorios tio! :o'
		);
	}	

	echo json_encode( $msg );
?>