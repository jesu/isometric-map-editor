<?php
// Evaluación
	if( $_POST && $_POST['name'] ){

		$name = $_POST['name'];
// Se eliminar del fichero

		$fichero = '../map/maps/'.$name.'.json';

		if( !file_exists( $fichero ) ){
			$msg = array(
				'success' => false,
				'error' => 'ERROR al eliminar MAPA - El servidor no ha encontrdo un mapa con el nombre '.$name
			);
		}else{

			unlink( $fichero );

			$msg = array(
				'success' => true,
				'message' => 'Mapa '.$name.' eliminado correctamente'
			);
		}

	}else{
		$msg = array(
			'success' => false,
			'error' => 'ERROR al eliminar MAPA - Tiene que haber un nombre :('
		);
	}	

	echo json_encode( $msg );
?>