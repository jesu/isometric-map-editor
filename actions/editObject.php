<?php
// Take all the shit!
	$name = $_POST['name'];
	$demonElement = $_POST['demonElement'];
	$humanElement = $_POST['humanElement'];

// Evaluación
	if( $name && $name != 'false' 
		&& $demonElement && $demonElement != 'false' 
		&& $humanElement && $humanElement != 'false' ){

		$mapObject = array(
			'demon' => $demonElement,
			'human' => $humanElement
		);

		//echo json_encode($mapObject);

// Se añade en el fichero

		$fichero = '../map/mapElements.json';
		$mapDefObj = json_decode( file_get_contents( $fichero ), true );

		if( empty( $mapDefObj['objects'][ $name ] ) ){

			$mapDefObj['objects'][ $name ] = $mapObject;

			file_put_contents( $fichero, json_encode( $mapDefObj ) );

			$msg = array(
				'success' => true,
				'message' => 'El objecto de mapa '.$name.' no existia, asi que se ha creado como uno nuevo'
			);
		}else{

			$mapDefObj['objects'][ $name ] = $mapObject;

			file_put_contents( $fichero, json_encode( $mapDefObj ) );

			$msg = array(
				'success' => true,
				'message' => 'El objecto de mapa '.$name.' ha sido editado correctamente'
			);
		}

	}else{
		$msg = array(
			'success' => false,
			'error' => 'ERROR al editar OBJECTO DE MAPA - Tiene que haber un nombre :('
		);
	}	

	echo json_encode( $msg );
?>