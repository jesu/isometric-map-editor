<?php
// Evaluación
	if( $_POST && $_POST['name'] ){

		$name = $_POST['name'];
// Se eliminar del fichero

		$fichero = '../map/mapElements.json';
		$mapElObj = json_decode( file_get_contents( $fichero ), true );

		if( empty( $mapElObj['objects'][ $name ] ) ){
			$msg = array(
				'success' => false,
				'error' => 'ERROR al eliminar OBJECTO - El servidor no ha encontrdo un objeto de mapa con el nombre '.$name
			);
		}else{

			unset( $mapElObj['objects'][ $name ] );

			file_put_contents( $fichero, json_encode( $mapElObj ) );

			$msg = array(
				'success' => true,
				'message' => 'Objeto de mapa '.$name.' eliminado correctamente'
			);
		}

	}else{
		$msg = array(
			'success' => false,
			'error' => 'ERROR al eliminar OBJETO - Tiene que haber un nombre :('
		);
	}	

	echo json_encode( $msg );
?>