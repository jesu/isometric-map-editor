<?php
// Evaluación
	if( $_POST && $_POST['name'] ){

		$name = $_POST['name'];
// Se eliminar del fichero

		$fichero = '../map/mapElements.json';
		$mapElObj = json_decode( file_get_contents( $fichero ), true );

		if( empty( $mapElObj['elements'][ $name ] ) ){
			$msg = array(
				'success' => false,
				'error' => 'ERROR al eliminar ELEMENTO - El servidor no ha encontrdo un elemento con el nombre '.$name
			);
		}else{

			unset( $mapElObj['elements'][ $name ] );

			file_put_contents( $fichero, json_encode( $mapElObj ) );

			$msg = array(
				'success' => true,
				'message' => 'Elemento '.$name.' eliminado correctamente'
			);
		}

	}else{
		$msg = array(
			'success' => false,
			'error' => 'ERROR al eliminar ELEMENTO - Tiene que haber un nombre :('
		);
	}	

	echo json_encode( $msg );
?>