<?php

function resizeMatrix(  $oldMatrix, $oldSize, $size ){
	$newMatrix = array();
	$sizeDif = $oldSize - $size;
	// Create the new content
	$dimCells = 2 * $size + 1;
	for ( $r=0; $r < $dimCells; $r++ ) { 
		$row = array();
		for ( $c=0; $c < $dimCells; $c++ ) { 
			array_push( $row, array() );
		}
		array_push( $newMatrix, $row );
	}
	//Fill new content with old one into new cells
	for ( $r=0; $r < count( $oldMatrix ); $r++ ) { 
		$newR = $r - $sizeDif;
		if( $newR >= 0 && $newR < $dimCells ){

			for ( $c=0; $c < count( $oldMatrix[ $r ] ); $c++ ) { 

				$newC = $c - $sizeDif;
				if( $newC >= 0 && $newC < $dimCells ){
					//echo $newR." - ".$newC." -> ".$r.' - '.$c;
					$newMatrix[ $newR ][ $newC ] = $oldMatrix[ $r ][ $c ];
				}
			}
		}
	}
	return $newMatrix;
};



// Take all the shit!
	$fileName = $_POST['fileName'];
	$name = $_POST['name'];
	$size = $_POST['size'];
	$type = $_POST['type'];
	$parent = $_POST['parent'];

// Evaluación
	if( $fileName && $fileName != 'false' 
		&& $name && $name != 'false' 
		&& $size && $size != 'false'
		&& $type && $type != 'false'){
		
// Se añade en el fichero

		$fichero = '../map/maps/'.$name.'.json';

		if( !file_exists( $fichero ) ){

			$ficheroOrigen = '../map/maps/'.$fileName.'.json';

			$mapObj = json_decode( file_get_contents( $ficheroOrigen ), true );
			$mapObj[ 'name' ] = $name;
			$mapObj[ 'type' ] = $type;
			$mapObj[ 'parent' ] = '';

			if( $type && $type != 'false' ){
				$mapObj[ 'parent' ] = $parent;
			}

			if( $mapObj[ 'size' ] != $size ){
				$newContent = resizeMatrix( $mapObj[ 'content' ], $mapObj[ 'size' ], $size  );
				$mapObj[ 'content' ] = $newContent;
			}

			$mapObj[ 'size' ] = $size;

			$fp = fopen( $fichero, "wb" );
			fwrite( $fp, json_encode( $mapObj ) );
			fclose( $fp );

			$msg = array(
				'success' => true,
				'message' => 'El mapa '.$name.' no existia, asi que se ha creado como un mapa nuevo',
				'mapName' => $name
			);
		}else{

			$mapObj = json_decode( file_get_contents( $fichero ), true );

			$mapObj[ 'name' ] = $name;
			$mapObj[ 'type' ] = $type;
			$mapObj[ 'parent' ] = '';

			if( $type && $type != 'false' ){
				$mapObj[ 'parent' ] = $parent;
			}

			if( $mapObj[ 'size' ] != $size ){
				$newContent = resizeMatrix( $mapObj[ 'content' ], $mapObj[ 'size' ], $size  );
				$mapObj[ 'content' ] = $newContent;
			}

			$mapObj[ 'size' ] = $size;

			file_put_contents( $fichero, json_encode( $mapObj ) );

			$msg = array(
				'success' => true,
				'message' => 'Nuevo mapa '.$name.' guardado correctamente',
				'mapName' => $name
			);
		}

	}else{
		$msg = array(
			'success' => false,
			'error' => 'ERROR al crear NUEVO MAPA - Todos los campos son obligatorios tio! :o'
		);
	}	

	echo json_encode( $msg );
?>