<?php
// Take all the shit!
	$name = $_POST['name'];
	$demonElement = $_POST['demonElement'];
	$humanElement = $_POST['humanElement'];

// Evaluación
	if( $name && $name != 'false' 
		&& ( $demonElement && $demonElement != 'false' || $humanElement && $humanElement != 'false' ) ){

		$mapObject = array(
			'demon' => $demonElement,
			'human' => $humanElement
		);

// Se añade en el fichero

		$fichero = '../map/mapElements.json';
		$mapDefObj = json_decode( file_get_contents( $fichero ), true );

		if( !empty( $mapDefObj[ 'objects' ][ $name ] ) ){
			$msg = array(
				'success' => false,
				'error' => 'ERROR al crear NUEVO OBJECTO DE MAPA - Ya existe un objecto de mapa con el nombre '.$name
			);
		}else{

			$mapDefObj[ 'objects' ][ $name ] = $mapObject;

			file_put_contents( $fichero, json_encode( $mapDefObj ) );

			$msg = array(
				'success' => true,
				'message' => 'Nuevo objeto de mapa '.$name.' creado correctamente'
			);
		}

	}else{
		$msg = array(
			'success' => false,
			'error' => 'ERROR al crear NUEVO OBJECTO DE MAPA - Tiene que haber un nombre y al menos un elemento seleccionado para una de la dos razas :('
		);
	}	

	echo json_encode( $msg );
?>