<?php
		$fichero = '../map/mapElements.json';

		// Si no existe se crea ahora mismo
		if ( !file_exists( $fichero ) ){

			$content = '{"elements":{},"objects":{}}';
			$fp = fopen( $fichero,"wb" );
			fwrite($fp,$content);
			fclose($fp);

		}	

		// Get Elements from File
			$mapElObj = json_decode( file_get_contents( $fichero ), true );
			$elements = $mapElObj[ 'elements' ];
			$objects = $mapElObj[ 'objects' ];
		// Get List of images from folder
			$dir = '../map/images';
			$imgList = array();

			if ( is_dir( $dir ) ) {
			    if ( $dh = opendir( $dir ) ) {
			        while ( ( $file = readdir( $dh ) ) !== false ) {
			        	if( strlen( $file ) > 4 ){
			            	array_push( $imgList, $file );
			            }
			        }
			        closedir( $dh );
			    }
			}

			$response = array(
				'elements' => $elements,
				'objects' => $objects,
				'images' => $imgList
			);

		echo json_encode( $response );
?>