<?php

$name = $_POST['name'];
$oldName = $_POST['oldName'];
$error = '';
$imagesPath = '../map/images/';


if( array_key_exists ( 'image' , $_FILES ) ){
	// Check for errors
	if($_FILES['image']['error'] > 0){
	    $error = 'Parece que ha habido un error mientras se subia la imagen';
	}

	if( !getimagesize( $_FILES['image']['tmp_name'] ) ){
	    $error = 'Por favor, asegúrate que estas subiendo una imagen';
	}
	/*
	// Check filetype
	if($_FILES['image']['type'] != 'image/png'){
	    $error = 'Unsupported filetype uploaded.';
	}
	*/
	// Check filesize
	if( $_FILES['image']['size'] > 500000 ){
	    $error = 'El fichero excede el tamaño, prueba con algo más pequeño o pregunta qué hacer a tu programador de confianza.';
	}

	$newName = $oldName;

	// Delete current edit file
	if( file_exists( $imagesPath . $oldName ) ){
		unlink ( $imagesPath . $oldName );
	}

	if( $name != $oldName ){
		$fileNameParts =  explode( ".", $_FILES['image']['name'] );

		if( !empty( $name ) ){
			$nameParts = explode(".", $name );
			$newName = $nameParts[0] . '.' . end( $fileNameParts );
		}else{
			$newName = $_FILES['image']['name'];
		}
	}

	// Check if the file exists
	if( file_exists( $imagesPath . $newName ) ){
		unlink ( $imagesPath . $newName );
	}
	// Upload file
	if( !move_uploaded_file( $_FILES['image']['tmp_name'], $imagesPath . $newName ) ){
	    $error = 'Error cargado el fichero, parece que no hay permisos de escritura';
	}

	// Success!
	if( empty($error) ){
		$msg = array(
			'success' => true,
			'message' => '¡La imagen se ha actualizado a la colección correctamente!'
		);
	}else{
		$msg = array(
			'success' => false,
			'error' => $error
		);
	}

}else{

	if( $name != $oldName ){
		if( !empty( $name ) ){
			$fileNameParts =  explode( ".", $oldName );
			$nameParts = explode(".", $name );
			$newName = $nameParts[0] . '.' . end( $fileNameParts );

			rename( $imagesPath . $oldName, $imagesPath . $newName );
			$msg = array(
				'success' => true,
				'message' => 'El nombre de la imagen ha sido cambiado a '.$newName
			);
		}else{
			$msg = array(
				'success' => false,
				'error' => 'No había imagen y has el nombre en blanco... el servidor pasa de ti un poco... :s'
			);
		}
	}else{
		$msg = array(
			'success' => false,
			'error' => 'No había imagen y has dejado el mismo nombre que tenia... vamos que no se ha hecho nada :s'
		);
	}

}


echo json_encode( $msg );

?>