<?php

	if( !empty( $_POST ) && $_POST[ 'name' ] && $_POST[ 'content' ] ){

		$name = $_POST[ 'name' ];
		$content = $_POST[ 'content' ];
		$fichero = '../map/maps/'.$name.'.json';

		if( !file_exists( $fichero ) ){

			$response = array(
				'success' => false,
				'error' => 'ERROR al guardar el contenido del mapa - No existe un mapa con el nombre '.$name
			);

		}else{

			$mapJSONData = json_decode( file_get_contents( $fichero ), true );
			$mapJSONData['content'] = json_decode($content);

			file_put_contents( $fichero, json_encode( $mapJSONData ) );

			$response = array(
				'success' => true,
				'name' => $name,
				'message' => 'Mapa '.$name.' salvado correctamente'
			);
		}
	}else{
		$response = array(
			'success' => false,
			'error' => 'ERROR al guardar el contenido del mapa - No hay nombre especificado o contenido :o'
		);
	}	
	
	echo json_encode( $response );
?>