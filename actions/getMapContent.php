<?php

	if( !empty( $_POST ) && $_POST[ 'name' ] ){

		$name = $_POST[ 'name' ];
		$fichero = '../map/maps/'.$name.'.json';

		if( !file_exists( $fichero ) ){

			$response = array(
				'success' => false,
				'error' => 'ERROR al cargar el contenido del mapa - No existe un mapa con el nombre '.$name
			);

		}else{

			$mapJSONData = json_decode( file_get_contents( $fichero ), true );
			$mapParentContent = [];

			// PARENT CONTENT
			if( array_key_exists( "parent", $mapJSONData ) ){

				$parentName = $mapJSONData[ "parent" ];
				$fichero = '../map/maps/'.$parentName.'.json';

				if( file_exists( $fichero ) ){

					// Add Parent Content
					$parentMapData = json_decode( file_get_contents( $fichero ), true );
					

					// GRAND PARENT CONTENT
					if( array_key_exists( "parent", $parentMapData ) ){


						$grandParentName = $parentMapData[ "parent" ];
						$fichero = '../map/maps/'.$grandParentName.'.json';

						if( file_exists( $fichero ) ){
							$grandParentMapData = json_decode( file_get_contents( $fichero ), true );

							$parentContent = $parentMapData[ "content" ];
							$mapParentContent = $grandParentMapData[ "content" ];

							for ( $i=0; $i < count( $mapParentContent ); $i++ ) { 

								for ( $j=0; $j < count( $mapParentContent[ $i ] ); $j++ ) { 
							
									for ($c=0; $c < count( $parentContent[ $i ][ $j ] ); $c++) { 
										array_push( $mapParentContent[ $i ][ $j ], $parentContent[ $i ][ $j ][ $c ] );
									}
								}
							}
						}else{
							$mapParentContent = $parentMapData[ "content" ];
						}
					}else{
						$mapParentContent = $parentMapData[ "content" ];
					}
				}
			}

			$map = array(
				"name" => $mapJSONData[ "name" ],
				"content" => $mapJSONData[ "content" ],
				"parent" => $mapParentContent
			);

			$response = array(
				'success' => true,
				'map' => $map,
				'message' => 'Mapa '.$name.' cargado correctamente'
			);
		}
	}else{
		$response = array(
			'success' => false,
			'error' => 'ERROR al cargar el contenido del mapa - No hay nombre especificado :o'
		);
	}	
	
	echo json_encode( $response );
?>