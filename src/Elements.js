define( 
    ['hbs!../../templates/window',
     'hbs!../../templates/elementsList',
     'hbs!../../templates/elementsNew',
     'hbs!../../templates/elementsEdit',
     'hbs!../../templates/elementData'],
    function( baseTemplate, elementsListTemplate, elementsNewTemplate, elementsEditTemplate, elementDataTemplate ) {

    	var templates = {
            base: baseTemplate,
            elementsList: elementsListTemplate,
            elementsNew: elementsNewTemplate,
            elementsEdit: elementsEditTemplate,
            elementData: elementDataTemplate
        };

    	var ElementsController = function( app ){
    		var _app = app;
            var _title = 'Elementos de mapa';
            var _key = 'element';
    		var _templates = templates;
            var _position = {
                x: 10,
                y: 40
            }
            this._el = null;
    		this._currentElementSelected = '';

    		/****************************************** INIT *************************************/
            this.init = function(){
                // HTML
                    var div = document.createElement( 'div' );
                    div.innerHTML = _templates.base( { key: _key, title: _title } );
                    this._el = div.childNodes[ 0 ];
                    this.dragController = new _app.utils.DraggableWindow( this._el, _position.x, _position.y );
                    document.body.appendChild( this._el );
                // Elements LIST
                    this.resetNewElementForm();
                // Events
                    this._el.querySelector('button.close').addEventListener('click', this.toggleShow.bind(this), false);
                    document.querySelector('.elements .elements_actions').addEventListener('click', this.onElementsActionPress.bind(this), false);
                    document.querySelector('.elements_list_wrap').addEventListener('click', this.onElementsListPress.bind(this), false);
                    document.querySelector('.elements_new_wrap').addEventListener('click', this.onElementsNewPress.bind(this), false);
                    document.querySelector('.elements_edit_wrap').addEventListener('click', this.onElementsEditPress.bind(this), false);
                    document.querySelector('#view_element_data').addEventListener('click', this.onElementsDataPress.bind(this), false);

                   _app.addEventListener( 'elementsUpdate', this.update.bind( this ) );

                return this;

            }.bind( this );
            /****************************************** INIT *************************************/


            this.update = function(){
            	this.updateElementsList();
            	this.setElementToEdit();
            };

    		/****************************************** UPDATE ELEMENT SECTIONS  *************************************/

            this.updateElementsList = function(){
            	var mapEls = _app.model.mapElements;
            	if( mapEls ){
                	document.querySelector('#elements_list').innerHTML = _templates.elementsList( { "elements": mapEls, "selected": this._currentElementSelected } );
                	this.updateElementsListData();
                }
            };

            this.updateElementsListData = function(){
            	var mapEls = _app.model.mapElements;

                if( this._currentElementSelected && mapEls[ this._currentElementSelected ] ){
                    var elData = mapEls[ this._currentElementSelected ];
                    elData.name = this._currentElementSelected;
                    var editButton = document.querySelector('[data-action=edit_element]');
                    if( editButton ){ editButton.onclick = null;}

                    document.querySelector('#view_element_data').innerHTML = _templates.elementData( elData );
                    editButton = document.querySelector('[data-action=edit_element]').onclick = this.onElementsActionPress.bind(this);

                }else{
                    var editButton = document.querySelector('[data-action=edit_element]');
                    if( editButton ){ editButton.onclick = null;}

                    document.querySelector('#view_element_data').innerHTML = '';
                }
            };

            this.setElementToEdit = function(){
            	var mapEls = _app.model.mapElements;

                if( this._currentElementSelected && mapEls[ this._currentElementSelected ] ){
                    var imgInput = document.querySelector('#edit_element_image');
                    var typeSel = document.querySelector('#edit_element_type');
                    if( imgInput ){ imgInput.onchange = null; }
                    if( typeSel ){ typeSel.onchange = null; }

                    var data = {
                        element: mapEls[ this._currentElementSelected ],
                        name: this._currentElementSelected,
                        images: _app.model.images,
                        config: _app.model.config,
                        mapElements: _app.model.mapElements
                    }
                    document.querySelector('.elements_edit_wrap').innerHTML = _templates.elementsEdit( data );
                    document.querySelector('#edit_element_image').onchange = this.onElementImageChange.bind(this);
                    document.querySelector('#edit_element_type').onchange = this.onElementTypeChange.bind(this);
                }
            };

            this.resetNewElementForm = function(){
                var imgInput = document.querySelector('#new_element_image');
                var typeSel = document.querySelector('#new_element_type');
                if( imgInput ){ imgInput.onchange = null; }
                if( typeSel ){ typeSel.onchange = null; }

                var data = {
                    images: _app.model.images,
                    config: _app.model.config,
                    mapElements: _app.model.mapElements
                };
                document.querySelector('.elements_new_wrap').innerHTML = _templates.elementsNew( data );
                document.querySelector('#new_element_image').onchange = this.onElementImageChange.bind(this);
                document.querySelector('#new_element_type').onchange = this.onElementTypeChange.bind(this);
            };
            /****************************************** UPDATE ELEMENT SECTIONS *************************************/

            /****************************************** PRESS EVENTS *************************************/
    		/**
             * onElementsActionPress
             *
             * Eventos del Menu de Elementos
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onElementsActionPress = function( e ){
                var tAction = _app.isActionEvent( e );
                if( tAction ){
                    this.elementsGoTo( tAction );
                }
            };
            /**
             * elementsGoTo
             *
             * Changes the subSection viewed into the Elements section
             * @param  {String} sectionID [ Section id]
             */
            this.elementsGoTo = function( sectionID ){
                var sections = document.querySelectorAll('.elements .action_sections > section');
                for (var i = sections.length - 1; i >= 0; i--) {
                    var button = document.querySelector('[data-action='+sections[i].id+']');
                    if( sections[i].id === sectionID ){
                        sections[i].classList.add('selected');
                        if( button ){
                            button.classList.add('selected');
                        }
                    }else{
                        sections[i].classList.remove('selected');
                        if( button ){
                            button.classList.remove('selected');
                        }
                    }
                }

                if( sectionID === 'new_element'){
                    this.resetNewElementForm();
                }
                if( sectionID === 'edit_element'){
                    this.setElementToEdit();
                }
            }
            /**
             * onListPress
             *
             * Eventos de Lista de Elementos
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onElementsListPress = function( e ){
                if( e.target.dataset && e.target.dataset.select ){
                    
                    var elList = document.querySelectorAll( '#elements_list li' );
                    var selectIdx = e.target.dataset.index;

                    if( elList[ selectIdx ].classList.contains('selected') ){
                        this._currentElementSelected = null;
                        elList[ selectIdx ].classList.remove('selected');
                    }else{
                        for (var i = 0; i < elList.length; i++) {
                            elList[i].classList.remove('selected');
                        }
                        elList[ selectIdx ].classList.add('selected');
                        this._currentElementSelected = e.target.dataset.select;
                    } 
                    this.updateElementsListData(); 
                }
            };
            /**
             * onElementsNewPress
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onElementsNewPress = function( e ){
                var tAction = _app.isActionEvent( e );
                this._onFormTAction( tAction, 'new' );
            };
            /**
             * onElementsEditPress
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onElementsEditPress = function( e ){
                var tAction = _app.isActionEvent( e );
                this._onFormTAction( tAction, 'edit' );
            };
            /**
             * _onFormTAction
             * 
             * @param  {[type]} tAction [Action to performed by a form button]
             * @param  {[type]} form    [Form button is from]
             */
            this._onFormTAction = function( tAction, form ){
                if( tAction === form + 'ElementAddImage' ){
                    var imgName = document.getElementById(form + '_element_image').value;
                    var elType = document.getElementById(form + '_element_type').value;
                    if( imgName ){
                        var srcList = document.getElementById(form + '_element_src');
                        var isSingle = ( elType === 'image' || elType === 'light' );
                        // Multiples imágenes solo para caso de tipo "image" o tipo "light", isSingle === true
                        if( isSingle && !srcList.value.length || !isSingle ){
                            if( srcList.value.length ){
                                srcList.value += ',';
                            }
                            srcList.value += imgName;
                            document.getElementById(form + '_element_src_label').innerHTML = srcList.value.replace(/,/g,',<br/>');
                        }
                    }  
                }
                if( tAction === form + 'ElementRemoveImage' ){ 
                    var srcList = document.getElementById(form + '_element_src');
                    if( srcList.value.length ){
                        var imgComp = srcList.value.split(',');
                        imgComp.pop();
                        srcList.value = imgComp.toString();
                        document.getElementById(form + '_element_src_label').innerHTML = srcList.value.replace(/,/g,',<br/>');
                    }
                }
                /************* SUBELEMENTS ****************/
                if( tAction === form + 'ElementAddSubelement' ){
                    var sElName = document.getElementById(form + '_element_elements').value;
                    if( sElName ){
                        var sElList = document.getElementById(form + '_element_subelements');
                        if( sElList.value.length ){
                            sElList.value += ',';
                        }
                        sElList.value += sElName;
                        document.getElementById(form + '_element_subelements_label').innerHTML = sElList.value.replace(/,/g,',<br/>');
                    }  
                }
                if( tAction === form + 'ElementRemoveSubelement' ){ 
                    var sElList = document.getElementById(form + '_element_subelements');
                    if( sElList.value.length ){
                        var sElComp = sElList.value.split(',');
                        sElComp.pop();
                        sElList.value = sElComp.toString();
                        document.getElementById(form + '_element_subelements_label').innerHTML = sElList.value.replace(/,/g,',<br/>');
                    }
                }
                /************* SUBMIT FORM ****************/
                if( tAction === form + 'Element' ){
                    var formFields = document.querySelectorAll('[data-form=' + form + '_element]');
                    var fields = _app.extractFromFields( formFields );
                    _app.serverRequest( form + 'Element', fields, this.onElementServerResponse.bind(this) );
                }
            };

            /**
             * onElementsDataPress
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onElementsDataPress = function( e ){
                var tAction = _app.isActionEvent( e );
                if( tAction === 'removeElement' ){
                    this._removeElement();
                }
            };

            /**
             * onElementImageChange 
             *
             * Element Image change on a element form
             * 
             * @param  {Object} ie [Objeto de evento]
             */
            this.onElementImageChange = function( ie ){
                var imgName = ie.target.value;
                var form = ie.target.dataset.form.split('_')[0];
                document.querySelector( '#' + form + '_element_image_preview' ).src = _app.model.config.map.path + imgName;
            };

            this.onElementTypeChange = function( ie ){
                var typeName = ie.target.value;
                var form = ie.target.dataset.form.split('_')[0];
                var blendModeSel = document.getElementById( form + '_element_blend_mode' );

                if( typeName === 'light' || typeName === 'movieClipLight' || typeName === 'PLACEHOLDER' ){
                   blendModeSel.disabled = true;
                   blendModeSel.selectedIndex = 0;
                }else{
                   blendModeSel.disabled = false;
                }

                if( typeName === 'image' || typeName === 'light' ){
                    var imgList = document.getElementById( form + '_element_src' );
                    imgList.value = imgList.value.split(',')[0];
                    document.getElementById( form + '_element_src_label' ).innerHTML = imgList.value;
                }

                if( typeName === 'movieClip' || typeName === 'movieClipLight' ){
                    document.querySelector('#' + form + '_element_form .animation_speed_wrap').classList.remove('hidden');
                }else{
                    document.querySelector('#' + form + '_element_form .animation_speed_wrap').classList.add('hidden');
                }
            };

        /****************************************** PRESS EVENTS *************************************/



        /****************************************** AJAX ACTIONS *************************************/

            this._removeElement = function(){
                if( this._currentElementSelected && _app.model.mapElements[ this._currentElementSelected ]){
                    if( window.confirm("¿Quieres borrar el elemento de mapa " + this._currentElementSelected + '?') ){
                        _app.serverRequest( 'removeElement', {name: this._currentElementSelected}, this.onElementServerResponse.bind(this) );
                        this._currentElementSelected = null;
                    }
                }
            };

            this.onElementServerResponse = function( res ){
                var msg = 'Ha pasado algo pero no sabria decir el qué :o';
                if( res && res.success ){
                    msg  = res.message;
                    _app.requestElementsUpdate();
                    this.elementsGoTo( 'view_element' );
                }else if( res && res.error ){
                    msg = res.error;
                }
                alert( msg );
            };

            /****************************************** AJAX ACTIONS *************************************/

            this.toggleShow = function(){
                this._el.classList.toggle('visible');
            };
    	};

        return ElementsController;
    }
);