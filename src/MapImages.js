define( 
    ['hbs!../../templates/window',
    'hbs!../../templates/imagesList',
    'hbs!../../templates/imageData',
    'hbs!../../templates/imagesEdit',
    'hbs!../../templates/imagesNew'],
    function( baseTemplate, imagesListTemplate, imageDataTemplate, imagesEditTemplate, imagesNewTemplate ) {

        var templates = {
            base: baseTemplate,
            imagesList: imagesListTemplate,
            imageData: imageDataTemplate,
            imagesEdit: imagesEditTemplate,
            imagesNew: imagesNewTemplate
        };

    	var MapImagesController = function( app ){

            var _app = app;
            var _title = 'Imágenes';
            var _key = 'image';
            var _templates = templates;
            var _position = {
                x: 30,
                y: 70
            }
            this._el = null;
            this._currentImageSelected = null;


            /****************************************** INIT *************************************/
    		this.init = function(){
                // HTML
                    var div = document.createElement('div');
                    div.innerHTML = _templates.base( { key: _key, title: _title} );
                    this._el = div.childNodes[0];
                    this.dragController = new _app.utils.DraggableWindow( this._el, _position.x, _position.y );
                    document.body.appendChild( this._el );
                // Images LIST
                    this.update();
                    this.updateImagesListData();
                // Events
                    this._el.querySelector('button.close').addEventListener('click', this.toggleShow.bind(this), false);
                    document.querySelector('.images .images_actions').addEventListener('click', this.onImagesActionPress.bind(this), false);
                    document.querySelector('.images_list_wrap').addEventListener('click', this.onImagesListPress.bind(this), false);
                    document.querySelector('.images_new_wrap').addEventListener('click', this.onImagesNewPress.bind(this), false);
                    document.querySelector('.images_edit_wrap').addEventListener('click', this.onImagesEditPress.bind(this), false);
                    document.querySelector('#view_image_data').addEventListener('click', this.onImagesDataPress.bind(this), false);

                   _app.addEventListener( 'elementsUpdate', this.update.bind( this ) );

                return this;

            }.bind( this );

            /****************************************** INIT *************************************/


            this.update = function(){
                this.updateImagesList();
                this.setImageToEdit();
            };

            /****************************************** UPDATE ELEMENT SECTIONS  *************************************/

            this.updateImagesList = function(){
                var images = _app.model.images;
                if( images ){
                    document.querySelector('#images_list').innerHTML = _templates.imagesList( { "images": images, "selected": this._currentImageSelected } );
                }
            };

            this.updateImagesListData = function(){
                var editButton = document.querySelector('[data-action=edit_image]');
                if( editButton ){ editButton.onclick = null;}
                document.querySelector('#view_image_data').innerHTML = _templates.imageData({});
                editButton = document.querySelector('[data-action=edit_image]').onclick = this.onImagesActionPress.bind(this);
            };

            this.setImageToEdit = function(){
                var images = _app.model.images;

                if( this._currentImageSelected !== null && images[ this._currentImageSelected ] ){
                    var imgInput = document.querySelector('#edit_image_file');
                    var editForm = document.querySelector('#form_edit_image');
 
                    if( imgInput ){ imgInput.onchange = null; }
                    if( editForm ){ editForm.onsubmit = null; }

                    var data = {
                        name: images[ this._currentImageSelected ]
                    }
                    document.querySelector('.images_edit_wrap').innerHTML = _templates.imagesEdit( data );
                    document.querySelector('#edit_image_file').onchange = this.onEditImageChange.bind(this);
                    document.querySelector('#form_edit_image').onsubmit = this.onEditFormSubmit.bind(this);
                }
            };

            this.resetNewImageForm = function(){
                var imgInput = document.querySelector('#new_image_file');
                var newForm = document.querySelector('#form_new_image');

                if( imgInput ){ imgInput.onchange = null; }
                if( newForm ){ newForm.onsubmit = null; }

                document.querySelector('.images_new_wrap').innerHTML = _templates.imagesNew( {} );
                document.querySelector('#new_image_file').onchange = this.onNewImageChange.bind(this);
                document.querySelector('#form_new_image').onsubmit = this.onNewFormSubmit.bind(this);
            };

            /****************************************** UPDATE ELEMENT SECTIONS *************************************/

            /****************************************** PRESS EVENTS *************************************/
            /**
             * onImagesActionPress
             *
             * Eventos del Menu de Elementos
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onImagesActionPress = function( e ){
                var tAction = _app.isActionEvent( e );
                if( tAction ){
                    this.imagesGoTo( tAction );
                }
            };
            /**
             * imagesGoTo
             *
             * Changes the subSection viewed into the Images section
             * @param  {String} sectionID [ Section id]
             */
            this.imagesGoTo = function( sectionID ){
                var sections = document.querySelectorAll('.images .action_sections > section');
                for (var i = sections.length - 1; i >= 0; i--) {
                    var button = document.querySelector('[data-action='+sections[i].id+']');
                    if( sections[i].id === sectionID ){
                        sections[i].classList.add('selected');
                        if( button ){
                            button.classList.add('selected');
                        }
                    }else{
                        sections[i].classList.remove('selected');
                        if( button ){
                            button.classList.remove('selected');
                        }
                    }
                }

                if( sectionID === 'new_image'){
                    this.resetNewImageForm();
                }
                if( sectionID === 'edit_image'){
                    this.setImageToEdit();
                }
            }
            /**
             * onImagesListPress
             *
             * Eventos de Lista de Elementos
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onImagesListPress = function( e ){

                if( e.target.dataset && e.target.dataset.select ){
                    
                    var elList = document.querySelectorAll( '#images_list li' );
                    var selectIdx = e.target.dataset.select;

                    if( elList[ selectIdx ].classList.contains('selected') ){
                        this._currentImageSelected = null;
                        elList[ selectIdx ].classList.remove('selected');
                    }else{
                        for (var i = 0; i < elList.length; i++) {
                            elList[i].classList.remove('selected');
                        }
                        elList[ selectIdx ].classList.add('selected');
                        this._currentImageSelected = e.target.dataset.select;
                    } 
                    this.updateImagesListData();
                }
            };

            /**
             * onImagesDataPress
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onImagesDataPress = function( e ){
                var tAction = _app.isActionEvent( e );
                if( tAction === 'removeImage' ){
                    this._removeImage();
                }
            };

            /**
             * onImagesNewPress
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onImagesNewPress = function( e ){
                var tAction = _app.isActionEvent( e );
                this._onFormTAction( tAction, 'new' );
            };
            /**
             * onImagesEditPress
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onImagesEditPress = function( e ){
                var tAction = _app.isActionEvent( e );
                this._onFormTAction( tAction, 'edit' );
            };

            this._onFormTAction = function( tAction, type ){

            };

            /**
             * onNewImageChange 
             * 
             * @param  {Object} ie [Objeto de evento]
             */
            this.onNewImageChange = function( e ){
                var input = e.target;
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        document.querySelector( '#new_image_preview' ).src = e.target.result;
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            };
            /**
             * onEditImageChange 
             * 
             * @param  {Object} ie [Objeto de evento]
             */
            this.onEditImageChange = function( e ){
                var input = e.target;
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        document.querySelector( '#edit_image_preview' ).src = e.target.result;
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            };

            this.onEditFormSubmit = function( e ){
                e.stopPropagation();
                e.preventDefault();
                this._uploadImage( 'edit' );
            };

            this.onNewFormSubmit = function( e ){
                e.stopPropagation();
                e.preventDefault();
                this._uploadImage( 'new' );
            };

            this._uploadImage = function( form ){
                var btn = document.getElementById('upload_' + form + '_image_button');
                btn.disabled = true;
                btn.classList.add('disabled');

                var _file = document.getElementById( form +'_image_file' );
                var _name = document.getElementById( form +'_image_name' );
                var _oldName = document.getElementById( form +'_image_old_name' );

                var data = new FormData();
                data.append('image', _file.files[0] );
                data.append('name', _name.value );
                data.append('oldName', _oldName.value );

                var request = new XMLHttpRequest();
                request.onreadystatechange = function(){
                    if(request.readyState == 4){
                        try {
                            var resp = JSON.parse( request.response );
                        } catch (e){
                            var resp = {
                                success: false,
                                error: 'Unknown error occurred: [' + request.responseText + ']'
                            };
                        }
                        this.onImageServerResponse( resp);
                    }
                }.bind( this );

                request.upload.addEventListener('progress', function(e){
                    var fChar = btn.innerHTML[1];
                    var nChar = '';

                    if( fChar === '^' || fChar === '\\' ){ nChar = '|'; }
                    if( fChar === '|' ){ nChar = '/'; }
                    if( fChar === '/' ){ nChar = '-'; }
                    if( fChar === '-' ){ nChar = '\\'; }
                    if( fChar === '\\' ){ nChar = '\\'; }

                    var btnText = '|' + nChar + '|' + Math.ceil(e.loaded/e.total) * 100 + '% |' + nChar + '|';

                    if( e.loaded === e.total ){ 
                        btnText = '|^| SUBIR |^|';
                        btn.disabled = false;
                        btn.classList.remove('disabled');
                    }

                    btn.innerHTML = btnText;
                }, false);

                request.open('POST', 'actions/' + form +'Image' + '.php');
                request.send(data);
            };

            /****************************************** PRESS EVENTS *************************************/



            /****************************************** AJAX ACTIONS *************************************/

            this._removeImage = function(){
                
                var imgName = _app.model.images[ this._currentImageSelected ];
                if( this._currentImageSelected !== null && imgName ){  
                    if( window.confirm("¿Quieres borrar la imagen " + imgName + '?') ){
                        _app.serverRequest( 'removeImage', { name: imgName }, this.onImageServerResponse.bind(this) );
                        this._currentImageSelected = null;
                    }
                }
            };

            this.onImageServerResponse = function( res ){
                var msg = 'Ha pasado algo pero no sabria decir el qué :o';
                if( res && res.success ){
                    msg  = res.message;
                    _app.requestElementsUpdate();
                    this.imagesGoTo( 'view_image' );
                }else if( res && res.error ){
                    msg = res.error;
                }
                alert( msg );
            };



            this.toggleShow = function(){
                this._el.classList.toggle('visible');
            };
    	};

            
    	
    	return MapImagesController;
    }
);