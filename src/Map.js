define( 
    ['src/lib/pixi-extended.js',
     'hbs!../../templates/window-map',
     'hbs!../../templates/mapsList',
     'hbs!../../templates/mapsNew',
     'hbs!../../templates/mapsEdit',
     'hbs!../../templates/mapData',
     'hbs!../../templates/mapContent',
     'hbs!../../templates/mapContentActions'],
    function( PIXI, baseTemplate, mapsListTemplate, mapsNewTemplate, mapsEditTemplate, mapDataTemplate, mapContentTemplate, mapContentActionsTemplate ) {

    	var templates = {
    		base: baseTemplate,
    		mapsList: mapsListTemplate,
    		mapsNew: mapsNewTemplate,
    		mapsEdit: mapsEditTemplate,
    		mapData: mapDataTemplate,
            mapContent: mapContentTemplate,
            mapContentActions: mapContentActionsTemplate
    	};
    	var _PIXI = PIXI;






    	var MapController = function( app ){

    		var _app = app;

    		var _templates = templates;
            var _title = 'Mapas';
            var _key = 'map';

    		// PIXI
                var PIXI = _PIXI;
                var _loader = new PIXI.loaders.Loader( _app.model.config.map.path );
                var _renderer = null;
                var _ticker = null;
                var _stage = null;
            // MAP
            	var _isoMap = null;
               	var _resolution = {         // Resolution configuration
                    w:960,                      // Current width resolution for the canvas
                    h:600,                      // Current height resolution for the canvas
                };
                var _assetsToLoad = [];
                var _elementsToInsert = [];
            // Content
                var _currentMapContent = {
                    coords:{
                        "i":0, 
                        "j":0 
                    },
                    aCoords:{
                        "i":0, 
                        "j":0  
                    },
                    objectSelected: null
                };
                var _currentRace = 'demon';
                var _layersState = {
                    grid: {
                        visible: true,
                        alpha: 0.5
                    },
                    parent: {
                        visible: true,
                        alpha: 1
                    }
                };
                var _zoom = 1;
            // HTML
            	var _position = {
            	    x: 5,
            	    y: 40
            	};
            	this._el = null;
            	this._currentMapSelected = null;

            /****************************************** UTILS *************************************/
            var _updateResolution = function(){
                var winWidth = window.innerWidth;
                var winHeight = window.innerHeight;
                var aspectRatio = winWidth / winHeight;
                _resolution.w = winWidth;
                _resolution.h = Math.ceil( _resolution.w / aspectRatio );
            };

            var _animate = function(time){
                PIXI.TWEEN.update();
                _renderer.render(_stage);
            };
            /****************************************** UTILS *************************************/

            this.init = function(){

            	_updateResolution();

            	// HTML
                    var div = document.createElement('div');
                    div.innerHTML = _templates.base( { key: _key, title: _title, zoom: _zoom} );
                    this._el = div.childNodes[0];
                    document.body.appendChild( this._el );
                     _position.x = _resolution.w - 300;
                    this.dragController = new _app.utils.DraggableWindow( this._el, _position.x, _position.y );
                // Events
                    this._el.querySelector('button.close').addEventListener('click', this.toggleShow.bind(this), false);
                    document.querySelector('.maps .maps_actions').addEventListener('click', this.onMapsActionPress.bind(this), false);
                    document.querySelector('.maps_list_wrap').addEventListener('click', this.onMapsListPress.bind(this), false);
                    document.querySelector('.maps_new_wrap').addEventListener('click', this.onMapsNewPress.bind(this), false);
                    document.querySelector('.maps_edit_wrap').addEventListener('click', this.onMapsEditPress.bind(this), false);
                    document.querySelector('#view_map_data').addEventListener('click', this.onMapsDataPress.bind(this), false);
                    document.querySelector('#maps_content_list').addEventListener('click', this.onMapsContentPress.bind( this ), false );
                    document.querySelector('.maps .map_content_actions').addEventListener('click', this.onMapsContentActionPress.bind(this), false);
                    document.querySelector('#map_content_coord_selected_i').addEventListener('change', this.onMapsContentCoordsChange.bind( this ), false );
                    document.querySelector('#map_content_coord_selected_j').addEventListener('change', this.onMapsContentCoordsChange.bind( this ), false );
                    document.querySelector('#map_content_zoom').addEventListener('click', this.onMapsZoomChange.bind( this ), false );
                    document.querySelector('#map_race_select').addEventListener('change', this.onMapsRaceChange.bind( this ), false );

                    document.querySelector('[data-action=closeMap]').addEventListener('click', this.closeMap.bind( this ), false );
                    document.querySelector('[data-action=saveMap]').addEventListener('click', this.saveMap.bind( this ), false );
                // Set initial configuration of the PIXI view
                    var rendererOptions = {
                        resolution: window.devicePixelRatio || 1
                    };
                    _renderer = PIXI.autoDetectRenderer( _resolution.w, _resolution.h, rendererOptions, false );
                    _renderer.roundPixels = true;
                    _renderer.isCanvas = ( _renderer instanceof PIXI.CanvasRenderer );

                    _stage = new PIXI.Container();
                    _ticker = new PIXI.ticker.Ticker();
                    _ticker.add(_animate);
                // Change scale mode everywhere.
                    PIXI.SCALE_MODES.DEFAULT = PIXI.SCALE_MODES.NEAREST;
                    //PIXI.Texture.fromImage("images/empty_pixel.png").baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
                // Inject the Canvas into the body
                    document.querySelector('#canvas-content').appendChild(_renderer.view);

                    _app.addEventListener( 'elementsUpdate', this.update.bind( this ) );
					_app.addEventListener( 'mapsUpdate', this.onMapsUpdate.bind( this ) );
                    _app.addEventListener( 'mapLoaded', this.onMapLoaded.bind( this ) );

                    for (var i = 0; i < _app.model.config.map.layers.length; i++) {
                        _layersState[ _app.model.config.map.layers[i] ] = {
                            visible: true,
                            alpha: 1
                        };                       
                    }

                return this;

            }.bind( this );


            this.update = function(){
            	this.onMapsUpdate();
                if( _app.model.mapLoaded ){
                    this.setMapContent();
                    this.setMapContentActions();
                    this.refreshMap();
                }
            };

            this.onMapsUpdate = function(){
                this.updateMapsList();
                this.setMapToEdit();
            };

            this.onMapLoaded = function(){
                this.create();
            };

            /****************************************** UPDATE MAP SECTIONS  *************************************/
            	this.updateMapsList = function(){
	            	var maps = _app.model.maps;
	            	if( maps ){
                        if( maps.length && this._currentMapSelected === null ){
                            this._currentMapSelected = 0;
                        }
	                	document.querySelector('#maps_list').innerHTML = _templates.mapsList( { "maps": maps, "selected": Number( this._currentMapSelected ) } );
	                	this.updateMapsListData();
	                }
	            };

	            this.updateMapsListData = function(){
	            	var maps = _app.model.maps;

	                if( this._currentMapSelected !== null && maps[ this._currentMapSelected] ){
	                    var mapData = maps[ this._currentMapSelected ];
	                    var editButton = document.querySelector('[data-action=edit_map]');
	                    if( editButton ){ editButton.onclick = null;}

	                    document.querySelector('#view_map_data').innerHTML = _templates.mapData( mapData );
	                    editButton = document.querySelector('[data-action=edit_map]').onclick = this.onMapsActionPress.bind(this);

	                }else{
	                    var editButton = document.querySelector('[data-action=edit_map]');
	                    if( editButton ){ editButton.onclick = null;}

	                    document.querySelector('#view_map_data').innerHTML = '';
	                }
	            };

	            this.setMapToEdit = function(){
	            	var maps = _app.model.maps;
	                if( this._currentMapSelected !== null && maps[ this._currentMapSelected ] ){
	                    var data = {
	                        map: maps[ this._currentMapSelected ],
	                        config: _app.model.config,
                            maps: _app.model.maps
	                    };
	                    document.querySelector('.maps_edit_wrap').innerHTML = _templates.mapsEdit( data );
	                }
	            };

	            this.resetNewMapForm = function(){
	                var data = {
	                    config: _app.model.config,
                        maps: _app.model.maps
	                }
	                document.querySelector('.maps_new_wrap').innerHTML = _templates.mapsNew( data );
	            };

                this.setMapContent = function(){
                    var cellContent = _app.getMapCellContent( _currentMapContent.aCoords );
                    if( cellContent ){
                        var elements = _app.model.mapElements;
                        var objects = _app.model.mapObjects;
                        var data = [];
                        for (var i = 0; i < cellContent.length; i++) {
                            var elName = objects[ cellContent[i] ][ _currentRace ];
                            data.push({
                                name: cellContent[i],
                                src: elements[ elName ].src,
                                layer: elements[ elName ].layer
                            });
                        }
                        document.querySelector('#maps_content_list').innerHTML = _templates.mapContent( { content: data, selected: _currentMapContent.objectSelected } );
                    }else{
                        document.querySelector('#maps_content_list').innerHTML = '';
                    }
                }

                this.setMapContentActions = function(){
                    var el = document.querySelector('.maps .map_content_actions');
                    if( el ){
                        data = {
                            objects: _app.model.mapObjects,
                            layers: _app.model.config.map.layers,
                            layersState: _layersState
                        };
                        el.innerHTML = _templates.mapContentActions( data );
                    }
                }


             /****************************************** UPDATE MAP SECTIONS  *************************************/

             /****************************************** PRESS EVENTS *************************************/
    		/**
             * onMapsActionPress
             *
             * Eventos del Menu de Mapas
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onMapsActionPress = function( e ){
                var tAction = _app.isActionEvent( e );
                if( tAction ){
                    this.mapsGoTo( tAction );
                }
            };
            /**
             * mapsGoTo
             *
             * Changes the subSection viewed into the Maps section
             * @param  {String} sectionID [ Section id]
             */
            this.mapsGoTo = function( sectionID ){
                var sections = document.querySelectorAll('.maps .action_sections > section');
                for (var i = sections.length - 1; i >= 0; i--) {
                    var button = document.querySelector('[data-action='+sections[i].id+']');
                    if( sections[i].id === sectionID ){
                        sections[i].classList.add('selected');
                        if( button ){
                            button.classList.add('selected');
                        }
                    }else{
                        sections[i].classList.remove('selected');
                        if( button ){
                            button.classList.remove('selected');
                        }
                    }
                }

                if( sectionID === 'new_map'){
                    this.resetNewMapForm();
                }

                if( sectionID === 'edit_map'){
                    this.setMapToEdit();
                }
            }
            /**
             * onListPress
             *
             * Eventos de Lista de Mapas
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onMapsListPress = function( e ){
                if( e.target.dataset && e.target.dataset.select ){
                    var selectValue = e.target.dataset.select;
                    var elList = document.querySelectorAll( '#maps_list li' );

                    if( elList[ selectValue ].classList.contains('selected') ){
                        this._currentMapSelected = null;
                        elList[ selectValue ].classList.remove('selected');
                    }else{
                        for (var i = 0; i < elList.length; i++) {
                            elList[i].classList.remove('selected');
                        }
                        elList[ selectValue ].classList.add('selected');
                        this._currentMapSelected = selectValue;
                    }    

                    this.updateMapsListData();
                }
            };
            /**
             * onMapsNewPress
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onMapsNewPress = function( e ){
                var tAction = _app.isActionEvent( e );
                if( tAction === 'newMap' ){
                    var formFields = document.querySelectorAll('[data-form=new_map]');
                	var fields = _app.extractFromFields( formFields );
                	_app.serverRequest( 'newMap', fields, this.onMapServerResponse.bind(this) );
                }
            };
            /**
             * onMapsDataPress
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onMapsDataPress = function( e ){
                var tAction = _app.isActionEvent( e );
                if( tAction === 'removeMap' ){
                    this._removeMap();
                }
                if( tAction === 'openMap' ){
                    this._openMap();
                }
            };
            /**
             * onMapsEditPress
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onMapsEditPress = function( e ){
                var tAction = _app.isActionEvent( e );
                if( tAction === 'editMap' ){
                    var formFields = document.querySelectorAll('[data-form=edit_map]');
                    var fields = _app.extractFromFields( formFields );
                    _app.serverRequest( 'editMap', fields, this.onMapEditResponse.bind(this) );
                }
            };


            this._addObjectToMap = function( objectID, i, j ){
                var mapObjectData = _app.model.mapObjects[ objectID ];
                if( mapObjectData ){
                    var elementID = mapObjectData[ _currentRace ];
                    if( elementID ){
                        var elementDataList = this._getElementDataFromID( elementID );
                        // ¿Existe el elemento al que apunta el objeto de mapa?
                        if( elementDataList ){
                            for (var e = 0; e < elementDataList.length; e++) {
                                _isoMap.addElement( elementID, i, j, elementDataList[e] );
                            }
                        }
                    }
                }   
            };

            /**
             * onMapsContentActionPress
             * @param  {[type]} e [description]
             */
            this.onMapsContentActionPress = function( e ){
                if( _app.model.mapLoaded ){
                    var tAction = _app.isActionEvent( e );

                    // ACCIONES DE BOTÓN
                    if( tAction ){
                        // Añade nuevo objecto a la celda del mapa seleccionada
                        if( tAction === "addObjectToContent" ){
                            var objectSel = document.getElementById('map_content_object_to_add').value;
                            var cellContent = _app.getMapCellContent( _currentMapContent.aCoords );
                            // ¿Existe esa celda en los contenidos y no hay ya un objeto así?
                            if( !_.isUndefined( cellContent ) &&  cellContent.indexOf( objectSel ) === -1 ){
                                _currentMapContent.objectSelected = cellContent.push( objectSel ) - 1;
                                _app.setMapCellContent( _currentMapContent.aCoords, cellContent );
                                this.setMapContent();
                                this._addObjectToMap( objectSel, _currentMapContent.coords.i, _currentMapContent.coords.j );
                                //this.refreshMap();
                            }
                        }
                        // Elimina el objeto que está señalado del contenido de una celda del mapa
                        if( tAction === "removeObjectFromContent" && _currentMapContent.objectSelected !== null ){
                            
                            var elList = document.querySelectorAll( '#maps_content_list li' );
                            var delEl = elList[ _currentMapContent.objectSelected ];
                            //¿Existe el elemento que se quiere eliminar en el DOM?
                            if( delEl && delEl.parentNode ){
                                var cellContent = _app.getMapCellContent( _currentMapContent.aCoords );
                                // ¿Existe el elemento en la lista de contenidos del mapa?
                                if( cellContent && cellContent[ _currentMapContent.objectSelected ] ){
                                    var objectID = cellContent[ _currentMapContent.objectSelected ];
                                    cellContent.splice( _currentMapContent.objectSelected, 1 );
                                    _app.setMapCellContent( _currentMapContent.aCoords, cellContent );
                                    _currentMapContent.objectSelected = ( _currentMapContent.objectSelected > 0 ) ? _currentMapContent.objectSelected - 1 : null;
                                    this.setMapContent();

                                    var elementID = _app.model.mapObjects[ objectID ][ _currentRace ];
                                    _isoMap.deleteElement( elementID, _currentMapContent.coords.i, _currentMapContent.coords.j );
                                    //this.refreshMap();
                                }
                            }
                        }
                        // Sube una posición el elemento seleccionado
                        if( tAction === "upObjectOnContent" && _currentMapContent.objectSelected !== null ){

                            var cellContent = _app.getMapCellContent( _currentMapContent.aCoords );
                            // ¿Existe el elemento en la lista de contenidos del mapa?
                            if( cellContent && _currentMapContent.objectSelected > 0 && cellContent[ _currentMapContent.objectSelected ] ){
                                // Move 1 position up the element
                                var el = cellContent.splice( _currentMapContent.objectSelected, 1 );
                                _currentMapContent.objectSelected--;
                                cellContent.splice( _currentMapContent.objectSelected, 0, el[0] );
                                // Update stuff
                                _app.setMapCellContent( _currentMapContent.aCoords, cellContent );
                                this.setMapContent();

                                _isoMap.deleteCellContent( _currentMapContent.coords.i, _currentMapContent.coords.j );
                                for (var i = cellContent.length - 1; i >= 0; i--) {
                                    this._addObjectToMap( cellContent[i], _currentMapContent.coords.i, _currentMapContent.coords.j );
                                }

                                //this.refreshMap();
                            }
                        }
                        // Baja una posición el elemento seleccionado
                        if( tAction === "downObjectOnContent" && _currentMapContent.objectSelected !== null ){

                            var cellContent = _app.getMapCellContent( _currentMapContent.aCoords );
                            // ¿Existe el elemento en la lista de contenidos del mapa?
                            if( cellContent && _currentMapContent.objectSelected < ( cellContent.length - 1 ) && cellContent[ _currentMapContent.objectSelected ] ){
                                // Move 1 position down the element
                                var el = cellContent.splice( _currentMapContent.objectSelected, 1 );
                                _currentMapContent.objectSelected++;
                                cellContent.splice( _currentMapContent.objectSelected, 0, el[0] );
                                // Update stuff
                                _app.setMapCellContent( _currentMapContent.aCoords, cellContent );
                                this.setMapContent();
                                _isoMap.deleteCellContent( _currentMapContent.coords.i, _currentMapContent.coords.j );
                                for (var i = cellContent.length - 1; i >= 0; i--) {
                                    this._addObjectToMap( cellContent[i], _currentMapContent.coords.i, _currentMapContent.coords.j );
                                }
                                //this.refreshMap();
                            }
                        }
                        // Cambia el alpha de una capa según el valor del input text asociado
                        if( tAction === "setLayerAlpha" ){
                            var layerName = e.target.dataset.layeraction;
                            var inputData = document.querySelector("[data-layeralpha=" + layerName + "]");
                            var alphaVal = Number( inputData.value );
                            alphaVal = ( alphaVal < 0 ) ? 0 : ( alphaVal > 1 ) ? 1 : alphaVal;
                            inputData.value = alphaVal.toString();

                            _layersState[layerName].alpha = alphaVal;
                            if( layerName === 'parent' ){
                                _isoMap.layers.parent.cacheAsBitmap = false;
                            }
                            _isoMap.layers[layerName].alpha = alphaVal;
                            if( layerName === 'parent' ){
                                _isoMap.layers.parent.cacheAsBitmap = true;
                            }
                        }
                    // ACCIONES DE VISIBILIDAD DE CAPA 
                    }else if( e && e.target && e.target.dataset && e.target.dataset.layer ){
                        var layerName = e.target.dataset.layer;
                        var active = !!e.target.checked;

                        if( _isoMap.layers[layerName] ){
                            _layersState[layerName].visible = active;
                            if( layerName === 'parent' ){
                                _isoMap.layers.parent.cacheAsBitmap = false;
                            }
                            _isoMap.layers[layerName].visible = active;
                            if( layerName === 'parent' ){
                                _isoMap.layers.parent.cacheAsBitmap = true;
                            }
                        }
                    }
                }
            }

            this.onMapsContentPress = function( e ){
                if( e && e.target && e.target.dataset ){
                    var selectValue = e.target.dataset.select;
                    var elList = document.querySelectorAll( '#maps_content_list li' );

                    if( elList[ selectValue ].classList.contains('selected') ){
                        _currentMapContent.objectSelected = null;
                    }else{
                        _currentMapContent.objectSelected = selectValue;
                        
                    } 
                    this._selectObjectInContentById( selectValue );                   
                }
            };

            this._selectObjectInContentById = function( idx ){
                var idx = idx || _currentMapContent.objectSelected;
                var elList = document.querySelectorAll( '#maps_content_list li' );
                for (var i = 0; i < elList.length; i++) {
                    elList[i].classList.remove('selected');
                }
                if( idx && elList [ idx ] ){
                    elList[ idx ].classList.add('selected');
                }
            };

            this.onMapsContentCoordsChange = function( e ){
                if( isNaN( e.target.value ) ){
                    e.target.value = "0";
                }
                this.setCurrentCellCoords();
                this.setMapContent();
            }

            this.onMapsZoomChange = function( e ){
                var tAction = _app.isActionEvent( e );
                if( tAction === 'map_zoom' ){
                    if( e.target.dataset.zoom === "-1" && _zoom > 0.25 ){
                        _zoom *= 0.5; 
                        _isoMap.scaleMap( _zoom );
                    }
                    if( e.target.dataset.zoom === "1" && _zoom < 4 ){
                        _zoom *= 2; 
                        _isoMap.scaleMap( _zoom );
                    }
                    document.querySelector('#map_zoom_value').value = _zoom;
                }
            };

            this.onMapsRaceChange = function( e ){
                _currentRace = e.target.value;
                if( _app.model.mapLoaded ){
                    this.refreshMap();
                }
            };

            /****************************************** PRESS EVENTS *************************************/


            /****************************************** AJAX ACTIONS *************************************/

            this._removeMap = function(){
                if( this._currentMapSelected !== null && _app.model.maps[ this._currentMapSelected ] ){
                	var name = _app.model.maps[this._currentMapSelected].name;
                	if( window.confirm("¿Quieres borrar el mapa " + name + '?') ){
                        if( _app.model.mapLoaded === name ){
                            this.closeMap();
                        }
                		_app.serverRequest( 'removeMap', {name: name }, this.onMapServerResponse.bind(this) );
                        this._currentMapSelected = null;
                	}
                }
            };

            this._openMap = function(){
                if( this._currentMapSelected !== null ){
                    var name = _app.model.maps[this._currentMapSelected].name;
                    if( _app.model.mapLoaded ){
                        this.closeMap();
                    }
                    _app.loadMapContent({name: name});
                }
            };

            this.onMapServerResponse = function( res ){
                var msg = 'Ha pasado algo pero no sabria decir el qué :o';
                if( res && res.success ){
                    msg  = res.message;
                    _app.requestMapsUpdate();
                    this.mapsGoTo( 'view_map' );
                }else if( res && res.error ){
                    msg = res.error;
                }
            };

            this.onMapEditResponse = function( res ){
                if( res && res.success ){
                    alert( res.message );
                    _app.requestMapsUpdate();
                    this.mapsGoTo( 'view_map' );
                    if( _app.model.mapLoaded && _app.model.mapLoaded === res.mapName ){
                        // Ha cambiado el tamaño del mapa o el parent??
                        this.closeMap();
                        _app.loadMapContent({name: res.mapName});
                    }
                }else{
                    this.onMapServerResponse(res);
                }
            };


            /****************************************** AJAX ACTIONS *************************************/

            this.create = function(){

                _zoom = 1;
                document.querySelector('#map_zoom_value').value = _zoom;

                if( this._currentMapSelected !== null ){
                    var cSize = ( _app.model.maps[this._currentMapSelected] ) ? _app.model.maps[this._currentMapSelected].size : _app.model.config.map.grid.n;
                	var mapOpt = {
                        ticker: _ticker,
                        mapScale: _zoom,
                        viewport: {
                            width: _resolution.w,
                            height: _resolution.h
                        },
                        grid: {
                            "n":  Number(cSize),
                            "tileSize": _app.model.config.map.grid.tileSize,
                            "bgColor": _app.model.config.map.grid.bgColor,
                            "bgVisible": _app.model.config.map.grid.bgVisible,
                            "bgCenter": _app.model.config.map.grid.bgCenter
                        },
                        path: _app.model.config.map.path
                    }
                    if( _isoMap ){
                        _isoMap.clear();
                    }
                    _isoMap = new PIXI.IsometricMap( _renderer, mapOpt, _app.debugMode );
                    _stage.addChild( _isoMap );
                    for( var lName in _layersState ){
                        _isoMap.layers[ lName ].visible = _layersState[ lName ].visible;
                        _isoMap.layers[ lName ].alpha = _layersState[ lName ].alpha;
                    }

                    this._loadMapObjects();
                    this.setMapContentActions();
                    this.setCurrentCellCoords();
                    this.setMapContent();
                    _isoMap.onMove = this.onMouseMove;
                    _isoMap.onMouseUp = this.onMapCellSelect.bind( this );

                    document.querySelector('#the_map_title').innerHTML = _app.model.maps[ this._currentMapSelected ].name;
                }
            };

            this._loadMapObjects = function(){
                var n = Number( _app.model.maps[ this._currentMapSelected ].size );
                var bgObject = _app.model.mapObjects['BACKGROUND'];
                var haveParent = !_.isEmpty( _app.model.mapParentContent );

                //Contenido de posible mapa padre
                if( haveParent ){
                    _isoMap.layers.parent.cacheAsBitmap = false;
                    
                    _isoMap.layers.parent.removeChildren();

                    for ( var i = -n; i <= n ; i++ ) {
                        for ( var j = -n; j <= n; j++ ) {
                            var parentContentIJ = _app.getMapCellContent( {"i": i + n, "j": j + n }, null, true );   
                            if( _.isArray( parentContentIJ ) ){ 
                
                                // Por cada objeto de mapa encontrado en las coordenadas de la celda...
                                for ( var o = 0; o < parentContentIJ.length; o++ ) { 
                                    var mapObjectData = _app.model.mapObjects[ parentContentIJ[ o ] ];
                                    var elementID = mapObjectData[ _currentRace ];
                                    var elementDataList = this._getElementDataFromID( elementID );
                                    // ¿Existe el elemento al que apunta el objeto de mapa?
                                    if( elementDataList ){
                                        for ( var e = 0; e < elementDataList.length; e++ ) {
                                            _isoMap.addElement( elementID, i, j, elementDataList[e], parent );
                                        }
                                    }                                
                                }
                            }
                        }
                    }
                    _isoMap.moveToParent();
                    _isoMap.layers.parent.cacheAsBitmap = true;
                }

                for ( var i = -n; i <= n ; i++ ){
                    for ( var j = -n; j <= n; j++ ){
                        // Posible background para el mapa
                        if( bgObject ){
                            _isoMap.addElement( 'BACKGROUND', i, j, _app.model.mapElements[ bgObject[ _currentRace ] ] );
                        }

                        // Pon el contenido que corresponda en la celda
                        var contentIJ = _app.getMapCellContent( {"i": i + n, "j": j + n } );   
                        if( _.isArray( contentIJ ) ){ 
                            // Por cada objeto de mapa encontrado en las coordenadas de la celda...
                            for ( var o = 0; o < contentIJ.length; o++ ) {     
                                this._addObjectToMap( contentIJ[ o ], i, j ); 
                            }
                        }
                    }
                } 

                _isoMap.layers.background.cacheAsBitmap = true;
                
                this.setMapContent();
                this.mapsGoTo('content_map');
                var n = Number( _app.model.maps[ this._currentMapSelected ].size );
                this.enable();

                _ticker.start();
                _isoMap.play();
            };

            this._getElementDataFromID = function( elementID ){
                var response = [];
                var elementData = _app.model.mapElements[ elementID ];

                if( elementID && elementData ){
                    // Canvas specific definition
                    if( !_.isEmpty( elementData.Canvas ) && _renderer.isCanvas ){
                        response = _.concat( response, this._getElementDataFromID( elementData.Canvas ) );
                    }else if( !_.isEmpty( elementData.WebGL ) && !_renderer.isCanvas ){
                    // WebGL specific definition
                        response = _.concat( response, this._getElementDataFromID( elementData.WebGL ) );
                    }else{
                        response.push( elementData );
                        var edse = elementData.subElements;
                        if( edse ){
                            for (var i = 0; i < edse.length; i++) {
                                response = _.concat( response, this._getElementDataFromID( edse[i] ) );
                            }
                        }
                    }
                }
                return response;
            };

            this.refreshMap = function(){
                _ticker.stop();
                _isoMap.stop();
                _isoMap.onClear = this._loadMapObjects.bind( this );
                _isoMap.clear();
            };

            this.closeMap = function(){
                _isoMap.onClear = null;
                _app.unloadMapContent();
                _currentMapContent.objectSelected =  null;
                document.querySelector('#the_map_title').innerHTML='';
                this.setMapContent();
                this.disable();
                this.destroy();
            };

            this.destroy = function(){
                if( _isoMap ){
                    _ticker.stop();
                    _isoMap.stop();
                    _isoMap.onMove = null;
                    _isoMap.clear();
                    _isoMap.removeChildren(0);
                    _stage.removeChildren(0);
                    _animate();
                } 
            };

            this.enable = function(){
                var btns = document.querySelectorAll('[data-needMap=true]');
                for (var i = 0; i < btns.length; i++) {
                    btns[i].classList.remove('disabled');
                }
            }
            this.disable = function(){
                var btns = document.querySelectorAll('[data-needMap=true]');
                for (var i = 0; i < btns.length; i++) {
                    btns[i].classList.add('disabled');
                }
            }


            this.onMouseMove = function(){};

            this.onMapCellSelect = function( cellCoords ){

                if( _.isArray( cellCoords ) && !isNaN( cellCoords[0] ) && !isNaN( cellCoords[1] ) ){
                    
                    document.querySelector('#map_content_coord_selected_i').value = cellCoords[0].toString();
                    document.querySelector('#map_content_coord_selected_j').value = cellCoords[1].toString();
                    this.setCurrentCellCoords();
                    this.setMapContent();
                }
            };

            this.setCurrentCellCoords = function(){
                var i = Number( document.querySelector('#map_content_coord_selected_i').value );
                var j = Number( document.querySelector('#map_content_coord_selected_j').value );

                if( !isNaN(i) && !isNaN(i) !== _currentMapContent.coords.i 
                    || j && j !== _currentMapContent.coords.j ){

                    _currentMapContent.objectSelected = null;
                
                    var n = 0;
                    if( this._currentMapSelected !== null ){
                        n = Number( _app.model.maps[ this._currentMapSelected ].size );

                        i = ( i < -n ) ? -n : ( i > n ) ? n : i;
                        j = ( j < -n ) ? -n : ( j > n ) ? n : j;
                        document.querySelector('#map_content_coord_selected_i').value = i.toString();
                        document.querySelector('#map_content_coord_selected_j').value = j.toString();
                    }
                    if( _.isNumber(i) ){
                        var i = Number( i );
                        _currentMapContent.coords.i = i;
                        _currentMapContent.aCoords.i = i + n;
                    }
                    if( _.isNumber(j) ){
                        var j = Number( j );
                        _currentMapContent.coords.j = j;
                        _currentMapContent.aCoords.j = j + n;
                    }
                    if( _isoMap ){
                        _isoMap.moveSelector( i, j );
                    }
                }
            };

            this.saveMap = function(){
                if( _app.model.mapLoaded ){
                    _app.saveMapContent();
                };
            }

            this.toggleShow = function(){
                this._el.classList.toggle('visible');
            };

    	};

        return MapController;
    }
);