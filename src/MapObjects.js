define( 
    ['hbs!../../templates/window',
     'hbs!../../templates/objectNew',
     'hbs!../../templates/objectEdit',
     'hbs!../../templates/objectsList',
     'hbs!../../templates/objectData'],
    function( baseTemplate, objectsNewTemplate, objectsEditTemplate, objectsListTemplate, objectDataTemplate ) {

    	var templates = {
            base: baseTemplate,
            objectsNew: objectsNewTemplate,
            objectsEdit: objectsEditTemplate,
            objectsList: objectsListTemplate,
            objectData: objectDataTemplate
        };

    	var MapObjectsController = function( app ){

    		var _app = app;
    		var _templates = templates;
    		var _title = 'Objectos de mapa';
            var _key = 'mapObject';
            var _position = {
                x: 20,
                y: 60
            }
            this._el = null;
    		this._currentObjectSelected = '';

    		/****************************************** INIT *************************************/
    		this.init = function(){
                // HTML
                    var div = document.createElement('div');
                    div.innerHTML = _templates.base( { key: _key, title: _title} );
                    this._el = div.childNodes[0];
                    this.dragController = new _app.utils.DraggableWindow( this._el, _position.x, _position.y );
                    document.body.appendChild( this._el );
               
                // Events
                    this._el.querySelector('button.close').addEventListener('click', this.toggleShow.bind(this), false);
                    document.querySelector('.mapObjects .mapObjects_actions').addEventListener('click', this.onObjectsActionPress.bind(this), false);
                    document.querySelector('.mapObjects_list_wrap').addEventListener('click', this.onObjectsListPress.bind(this), false);
                    document.querySelector('.mapObjects_new_wrap').addEventListener('click', this.onObjectsNewPress.bind(this), false);
                    document.querySelector('.mapObjects_edit_wrap').addEventListener('click', this.onObjectsEditPress.bind(this), false);
                    document.querySelector('#view_mapObject_data').addEventListener('click', this.onObjectsDataPress.bind(this), false);

                   _app.addEventListener( 'elementsUpdate', this.update.bind( this ) );

                return this;

            }.bind( this );
            /****************************************** INIT *************************************/


            this.update = function(){
            	this.updateObjectsList();
            	this.setObjectToEdit();
                this.resetNewObjectForm();
            };

            /****************************************** UPDATE OBJECT SECTIONS  *************************************/
            this.updateObjectsList = function(){
            	var mapObjs = _app.model.mapObjects;
                var mapEls = _app.model.mapElements;
            	if( mapObjs && mapEls ){
                    var data = {
                        "selected": this._currentObjectSelected,
                        "objects": mapObjs,
                        "elements": mapEls
                    }
                	document.querySelector('#mapObjects_list').innerHTML = _templates.objectsList( data );
                	this.updateObjectsListData();
                }
            };

            this.updateObjectsListData = function(){
            	var mapObjs = _app.model.mapObjects;

                if( this._currentObjectSelected && mapObjs[ this._currentObjectSelected ] ){

                    var editButton = document.querySelector('[data-action=edit_mapObject]');
                    if( editButton ){ editButton.onclick = null;}
                    var data = {
                        object: mapObjs[ this._currentObjectSelected ],
                        name: this._currentObjectSelected,
                        elements: _app.model.mapElements
                    }
                    document.querySelector('#view_mapObject_data').innerHTML = _templates.objectData( data );
                    editButton = document.querySelector('[data-action=edit_mapObject]').onclick = this.onObjectsActionPress.bind(this);

                }else{
                    this._currentObjectSelected = '';
                    var editButton = document.querySelector('[data-action=edit_mapObject]');
                    if( editButton ){ editButton.onclick = null;}
                    document.querySelector('#view_mapObject_data').innerHTML = '';
                }
            };

            this.setObjectToEdit = function(){
            	var mapObjs = _app.model.mapObjects;
                if( this._currentObjectSelected && mapObjs[ this._currentObjectSelected ] ){

                    var data = {
                        object: mapObjs[ this._currentObjectSelected ],
                        name: this._currentObjectSelected,
                        elements: _app.model.mapElements
                    }
                    document.querySelector('.mapObjects_edit_wrap').innerHTML = _templates.objectsEdit( data );
                }
            };

            this.resetNewObjectForm = function(){
                var data = {
                    elements: _app.model.mapElements
                };
                document.querySelector('.mapObjects_new_wrap').innerHTML = _templates.objectsNew( data );
            };
            /****************************************** UPDATE OBJECT SECTIONS *************************************/

            /****************************************** PRESS EVENTS *************************************/
    		/**
             * onObjectsActionPress
             *
             * Eventos del Menu de Objetos de Mapa
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onObjectsActionPress = function( e ){
                var tAction = _app.isActionEvent( e );
                if( tAction ){
                    this.elementsGoTo( tAction );
                }
            };
            /**
             * elementsGoTo
             *
             * Changes the subSection viewed into the Elements section
             * @param  {String} sectionID [ Section id]
             */
            this.elementsGoTo = function( sectionID ){
                var sections = document.querySelectorAll('.mapObjects .action_sections > section');
                for (var i = sections.length - 1; i >= 0; i--) {
                    var button = document.querySelector('[data-action='+sections[i].id+']');
                    if( sections[i].id === sectionID ){
                        sections[i].classList.add('selected');
                        if( button ){
                            button.classList.add('selected');
                        }
                    }else{
                        sections[i].classList.remove('selected');
                        if( button ){
                            button.classList.remove('selected');
                        }
                    }
                }
                if( sectionID === 'new_mapObject'){
                    this.resetNewObjectForm();
                }
                if( sectionID === 'edit_mapObject'){
                    this.setObjectToEdit();
                }

            }

            /**
             * onListPress
             *
             * Eventos de Lista de Elementos
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onObjectsListPress = function( e ){
                if( e.target.dataset && e.target.dataset.select ){

                    var elList = document.querySelectorAll( '#mapObjects_list li' );
                    var selectIdx = e.target.dataset.index;

                    if( elList[ selectIdx ].classList.contains('selected') ){
                        this._currentObjectSelected = null;
                        elList[ selectIdx ].classList.remove('selected');
                    }else{
                        for (var i = 0; i < elList.length; i++) {
                            elList[i].classList.remove('selected');
                        }
                        elList[ selectIdx ].classList.add('selected');
                        this._currentObjectSelected = e.target.dataset.select;
                    } 
                    this.updateObjectsListData();
                }
            };
            /**
             * onObjectsNewPress
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onObjectsNewPress = function( e ){
                var tAction = _app.isActionEvent( e );
                this._onFormTAction( tAction, 'new' );
            };
            /**
             * onElementsEditPress
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onObjectsEditPress = function( e ){
                var tAction = _app.isActionEvent( e );
                this._onFormTAction( tAction, 'edit' );
            };
            /**
             * _onFormTAction
             * 
             * @param  {[type]} tAction [Action to performed by a form button]
             * @param  {[type]} form    [Form button is from]
             */
            this._onFormTAction = function( tAction, form ){
                /************* SUBMIT FORM ****************/
                if( tAction === form + 'Object' ){
                    var formFields = document.querySelectorAll('[data-form=' + form + '_object]');
                    var fields = _app.extractFromFields( formFields );

                    _app.serverRequest( form + 'Object', fields, this.onObjectServerResponse.bind(this) );
                }
            };

            /**
             * onObjectsDataPress
             * 
             * @param  {Object} e [Objeto de evento]
             */
            this.onObjectsDataPress = function( e ){
                var tAction = _app.isActionEvent( e );
                if( tAction === 'removeObject' ){
                    this._removeObject();
                }
            };

        /****************************************** PRESS EVENTS *************************************/



        /****************************************** AJAX ACTIONS *************************************/

            this._removeObject = function(){
                if( this._currentObjectSelected && _app.model.mapObjects[ this._currentObjectSelected ] ){
                    if( window.confirm("¿Quieres borrar el objecto de mapa " + this._currentObjectSelected + '?') ){
                        _app.serverRequest( 'removeObject', { name: this._currentObjectSelected }, this.onObjectServerResponse.bind(this) );
                        this._currentObjectSelected = null;
                    }
                }
            };

            this.onObjectServerResponse = function( res ){
                var msg = 'Ha pasado algo pero no sabria decir el qué :o';
                if( res && res.success ){
                    msg  = res.message;
                    _app.requestElementsUpdate();
                    this.elementsGoTo( 'view_mapObject' );
                }else if( res && res.error ){
                    msg = res.error;
                }
                alert( msg );
            };

            /****************************************** AJAX ACTIONS *************************************/




            this.toggleShow = function(){
                this._el.classList.toggle('visible');
            };


    	};

    	return MapObjectsController;
    }
);