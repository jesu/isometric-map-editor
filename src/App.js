"use strict";
/**
 * NIGROMANCE 28/09/2016
 *
 * War of Ecleptia Map Editor*
 * 
 */
require.config(
    {
        paths: {
            handlebars: "lib/handlebars-v3.0.3",
            text: "lib/requirejs-text",
            hbs: "lib/requirejs-hbs"
        },
        shim: {
            handlebars: {
                exports: "Handlebars"
            }
        }
    }
);

define( 
    ['src/DraggableWindow.js',
     'src/Map.js',
     'src/Elements.js',
     'src/MapObjects.js',
     'src/MapImages.js',
     'handlebars',
     'text!Config.json',
     'hbs!../templates/base'],
    function( DraggableWindow, MapController, ElementsController, MapObjectsController, MapImagesController, Handlebars, Config, baseTemplate ) {

        /***** HANDLEBARS *****/
        var templates = {
            base: baseTemplate
        };

        Handlebars.registerHelper( 'ifCond', function( v1, v2, options ) {
          if(v1 === v2) {
            return options.fn(this);
          }
          return options.inverse(this);
        } );
        Handlebars.registerHelper( 'justOne', function( v1 ) {
          if( _.isArray(v1) ) {
            return v1[0];
          }else{
            return v1;
          }     
        } );
        Handlebars.registerHelper( 'forceList', function( v1 ) {
            var result = v1;
            if( _.isArray(v1) ) {
                result = v1[0];
                for (var i = 1; i < v1.length; i++) {
                    result += ',<br/>' + v1[i];
                }
            }
            return result;     
        } );
        Handlebars.registerHelper( 'forceText', function( v1 ) {
            var result = v1;
            if( _.isArray(v1) ) {
                result = v1[0];
                for (var i = 1; i < v1.length; i++) {
                    result += ',' + v1[i];
                }
            }
            return result;     
        } );
        Handlebars.registerHelper( 'getProp', function( v1, prop1, prop2, prop3 ) {
            var result = v1;
            if( typeof prop1 === "string" ){
                result = result[prop1];
            }
            if( typeof prop2 === "string" ){
                result = result[prop2];
            }
            if(typeof prop3 === "string" ){
                result = result[prop3];
            }

            if( _.isArray(result) ) {
                result = result[0];
            }
            return result;
        } );

        /***** HANDLEBARS *****/



        var App = function( PIXI, Config, templates ){
            
            var _eventListeners = {};
            var _AJAXPending = false;
            var _pendingRequests = [];

            this.model = {
                config: JSON.parse( Config )
            };
            this.debugMode = true;

            this.elementsController = null;
            this.mapController = null;
            this.mapImagesController = null;
            this.mapObjectsController = null;

            // HTML
                this._footerMsg = null;
                var _templates = templates;
            
            this.utils = {
                DraggableWindow: DraggableWindow
            };

            /******************************************** UTILS ******************************************************/
            var _encodeObject = function( o ){
                var resp = '';
                for( var f in o){
                    if( resp ){
                        resp += '&';
                    }
                    resp += f + '=' + o[f];                 
                }
                return resp;
            };

            var _broadcastEvent = function( eventName, data ){
                if( _eventListeners[ eventName ] ){
                    for (var i = 0; i < _eventListeners[ eventName ].length; i++) {
                        _eventListeners[ eventName ][i]( data );
                    }
                }
            };

            var _onMenuPress = function( e ){
                var tAction = this.isActionEvent( e );
                if( tAction === 'windowElements' && this.elementsController){
                    this.elementsController.toggleShow();
                }
                if( tAction === 'windowMaps' && this.mapController ){
                    this.mapController.toggleShow();
                }
                if( tAction === 'windowImages' && this.mapImagesController ){
                    this.mapImagesController.toggleShow();
                }
                if( tAction === 'windowObjects' && this.mapObjectsController ){
                    this.mapObjectsController.toggleShow();
                }
            };
            /****************************************** UTILS *************************************/

            this.addEventListener = function( eventName, callback ){
                if( _.isString( eventName ) && _.isFunction( callback ) ){
                    if( !_eventListeners[ eventName ] ){
                        _eventListeners[ eventName ] = [];
                    }
                    _eventListeners[ eventName ].push(callback);
                }
            };

            this.serverRequest = function( action, data, callback ){
                _pendingRequests.push( { action: action, data: data, callback: callback } );
                _AJAXRequest();
            };

            var _AJAXRequest = function( action, data, callback ){

                if ( !_AJAXPending && _pendingRequests.length ) {
                    var reqData = _pendingRequests.shift();
                    var xhr = new XMLHttpRequest();
                    var callback  = callback || function(){};


                    _AJAXPending = true;
                    var _currentAJAXCallback = function(){
                            var res = {};
                            _AJAXPending = false;
                            
                            //console.log(xhr.responseText);
                            try{  res = JSON.parse( xhr.responseText ); }
                            catch( err ){ res = err; }
                            reqData.callback( res );
                            _AJAXRequest();
                    }.bind( this );

                    xhr.open( 'POST', 'actions/' + reqData.action + '.php', true );
                    xhr.setRequestHeader( 'Content-Type', 'application/x-www-form-urlencoded' );

                    xhr.onloadend = _currentAJAXCallback;
                    xhr.onerror = _currentAJAXCallback;

                    xhr.send( _encodeObject( reqData.data ) );
                }
            };

            this.isActionEvent = function( e ){
                var response = false;
                 if( e && e.target && e.target.dataset ){
                    var target = e.target;
                    if( target.dataset.action || target.parentNode.dataset.action ){
                        var tAction = target.dataset.action || target.parentNode.dataset.action;
                        response = tAction;
                    }
                }
                return response;
            };

            this.extractFromFields = function( formFields ){
                var fields = {};
                for (var i = formFields.length - 1; i >= 0; i--) {
                    var value = formFields[i].value;
                    if( formFields[i].type === 'checkbox' ){
                        value = formFields[i].checked;
                    }
                    fields[ formFields[i].dataset.field ] = ( value !== '' ) ? value : false;
                }
                return fields;
            };
            /******************************************** UTILS ******************************************************/

            /****************************************** INIT *************************************/
            this.init = function(){
                // HTML
                    var div = document.createElement('div');
                    div.innerHTML = _templates.base({});
                    for (var i = div.childNodes.length - 1; i >= 0; i--) {
                        document.body.appendChild( div.childNodes[ i ] );
                    }
                // Controllers
                    this.elementsController = new ElementsController( this ).init();
                    this.mapImagesController = new MapImagesController( this ).init();
                    this.mapObjectsController = new MapObjectsController( this ).init();
                    this.mapController = new MapController( this ).init();
                // Header
                    document.querySelector('#app_menu').addEventListener( 'click', _onMenuPress.bind( this ), false );
                // Footer
                    this._footerMsg = document.querySelector('footer .message span');
                    this.mapController.onMouseMove = function( cellsCoords){
                        this._footerMsg.innerHTML = '  Celda: ['+cellsCoords.toString()+']';
                    }.bind(this);

                this.requestElementsUpdate();
                this.requestMapsUpdate();

                return this;

            }.bind( this );

            /*this.requestImagesUpdate = function(){
                 this.serverRequest( 'getImages', {}, function( res ){
                    this.model.images = res.images.sort(); 
                    _broadcastEvent( 'imagesUpdate', this.model.images );
                }.bind( this ) );
            }.bind( this );*/

            this.requestElementsUpdate = function(){
                 this.serverRequest( 'getMapElements', {}, function( res ){
                    this.model.mapElements = this.orderObject( res.elements );
                    this.model.mapObjects = this.orderObject( res.objects );
                    this.model.images = res.images.sort(); 
                    _broadcastEvent( 'elementsUpdate', this.model.mapElements );
                }.bind( this ) );
            }.bind( this );

            this.requestMapsUpdate = function(){
                 this.serverRequest( 'getMaps', {}, function( res ){
                    this.model.maps = res.maps;
                    _broadcastEvent( 'mapsUpdate', this.model.maps );
                }.bind( this ) );
            }.bind( this );

            this.loadMapContent = function( data ){
                this.serverRequest( 'getMapContent', data, function( res ){
                    this.model.mapContent = res.map.content;
                    this.model.mapParentContent = res.map.parent;
                    this.model.mapLoaded = res.map.name;
                    _broadcastEvent( 'mapLoaded', this.model.mapContent );
                }.bind( this ) );
            };

            this.unloadMapContent = function(){
                this.model.mapContent = null;
                this.model.mapParentContent = null;
                this.model.mapLoaded = '';
            }

            this.saveMapContent = function(){
                var data = {
                    "name": this.model.mapLoaded,
                    "content": JSON.stringify(this.model.mapContent)
                };
                this.serverRequest( 'saveMapContent', data, function( res ){
                    document.querySelector('[data-action=saveMap]').innerHTML = ' SAVE ';
                    _broadcastEvent( 'mapSaved', this.model.mapLoaded );
                }.bind( this ) );
            }; 

            this.getMapCellContent = function( coords, objIdx, parent ) {
                var response = [];
                var mc = ( parent ) ? this.model.mapParentContent : this.model.mapContent;

                if( mc && coords && mc[coords.i] && mc[coords.i][coords.j] ){
                    var cellContent = mc[coords.i][coords.j];
                    if( _.isNumber( objIdx ) ){
                        response = cellContent[ objIdx ];
                    }else{
                        response = cellContent.slice( 0 );
                    }
                }
                return response;
            };

            this.setMapCellContent = function( coords, content ){
                var mc = this.model.mapContent;
                document.querySelector('[data-action=saveMap]').innerHTML = ' SAVE*';
                if( coords && _.isArray( content ) && mc[coords.i] && mc[coords.i][coords.j] ){
                    mc[coords.i][coords.j] = content;
                }
            }
            
            /****************************************** INIT *************************************/

            this.orderObject = function( obj ){
                var result = null;
                if( _.isObject( obj ) ){
                    result = {};
                    var sortKeys = Object.keys( obj ).sort();
                    for (var i = 0; i < sortKeys.length; i++) {
                        result[ sortKeys[i] ] = obj [ sortKeys[i] ];
                    }
                }
                return result;
            };
        };

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var app = new App( PIXI, Config, templates ).init();

        return app;
    }
);