"use strict";
/****************************************/
/* 				Extend PIXI  			*/
/****************************************/
define(['src/lib/pixi.js', 'src/lib/Tween.js'],
	function( PIXI, TWEEN ){

		var shockwaveFilter = require( [ 'src/lib/pixi-filters/shockwave.js' ] );

		PIXI.isCocoonJS = window.navigator && window.navigator.isCocoonJS || false;
		PIXI.TWEEN = TWEEN;
		
		PIXI.isHEXColorString = function(value){
		    if(_.isString(value)){
		        return /^#([0-9a-f]{3}){1,2}$/i.test(value);
		    }else{
		        return false;
		    }
		};

		PIXI.isCanvasEvent = function( e ){
			if( e && e.data && e.data.originalEvent
				&& e.data.originalEvent.target
				&& e.data.originalEvent.target.tagName
				&& e.data.originalEvent.target.tagName.toUpperCase() === "CANVAS" ){
				return true;
			}else{
				return false;
			}
		};


		PIXI.TWEEN.Easing.ElasticWOE = {
			Out: function (k) {
				var s;
				var a = 0.9;
				var p = 1.1;

				if (k === 0) {
					return 0;
				}

				if (k === 1) {
					return 1;
				}

				if (!a || a < 1) {
					a = 1;
					s = p / 4;
				} else {
					s = p * Math.asin(1 / a) / (2 * Math.PI);
				}

				return (a * Math.pow(2, - 10 * k) * Math.sin((k - s) * (2 * Math.PI) / p) + 1);
			}
		};

		/**
		 * MouseWheelHandler
		 *
		 * Handler for the scroll event. Must to be set at the begining asigning
		 * the mouse wheel event on the canvas to the "onMouseWheel" callback function.
		 * Then, every time this callback is called, the stores callbacks by "addCallback" 
		 * function are called as event dipatcher.
		 * This stored callback should have an id so it could be deleted later by "removeCallback"
		 */
		PIXI.MouseWheelHandler = {
			callbacks: {},
			onMouseWheel: function(e){
				for (var cb in this.callbacks) {
					this.callbacks[cb](e);
				}
			},
			addCallback: function(id, callback, scope){
				var response = null;
				if( _.isFunction(callback) ){
					var cbId = '';
					if(id && _.isString(id)){
						cbId = id;
					}else{
						var date = +new Date();
						cbId = date.toString();
						response = cbId;
					}
					this.callbacks[cbId] = (scope) ? callback.bind(scope) : callback;	
				}
				return cbId;
			},
			removeCallback: function(id){
				if(this.callbacks[id]){
					delete this.callbacks[id];
				}
			}
		};


		/**
		 * IsometricMap
		 *
		 * Isometric map to be managed.
		 */
		PIXI.IsometricMap = function(renderer, opt, debugMode){

			PIXI.Container.call(this);

			this._path = opt.path || '';
			this.debugMode = debugMode || false; 					// Flag to activate the grid and show in console indexes coordinates
			this._ticker = opt.ticker || null;
			this._renderer = renderer || null;

			this._mapDragging = false; 					// Flag to mark whether there is dragging in process.
			this.viewport = opt.viewport || { width: 100, height: 100 };	// Viewport size where to see the map
			this._vel = {x: 0, y: 0};
			this._maxDisp = { x: 0, y: 0 }; 			// Maximum dispacement allow when map is moving. Calculate into "scaleMap" function
			this._minDisp = { x: 0, y: 0 }; 			// Minimun dispacement allow when map is moving. Calculate into "scaleMap" function
			this._dragOrigin = { x: 0, y: 0 }; 			// Original point of reference in draging
			this._viewCenter = { x: 0, y: 0 }; 			// Center coordinates of the map.
			this._mapScale = opt.mapScale || 1;			// scale

			var grid = opt.grid || {};
			this.n = opt.grid.n || 10;					// 2*n X 2*n cells Square
			this._tileSize = opt.grid.tileSize || 100;	// tile width
			this._tileGap = 0; 							// gap between tiles
			this._rotation = 1; 						// rotation of 1 gives you 45 Degrees
			this._isoCoef = 2; 							// 2 gives you isometric view
			this.offsetX = (this.n * 2 + 1) * this._tileSize; 			// w  / 2;    // offset
			this.offsetY = ( (this.n * 2 + 1) * this._tileSize - this._tileSize) / this._isoCoef; // offsetX / isoCoef // (h - tileWidth) / 2;
			this.bgColor = opt.grid.bgColor || '0XAAAAAA';
			this.bgCenterColor = opt.grid.bgCenter || '0XFFFFFF';
			this.bgVisibleColor = opt.grid.bgVisible || '0XCCCCCC';
			this.bg = null;
			this.groundCachedTexture = null;
			this.layers = {
				background: new PIXI.Container(),
				parent: new PIXI.Container(),
				ground: new PIXI.Container(),
				behind: new PIXI.Container(),
				decorations: new PIXI.Container(),
				front: new PIXI.Container(),
				grid: new PIXI.Container()
			};
			this._beingList = {};
			this.cellsContent = [];
			this.selectionTile = null;

			this._minDrawPosition = {
				x: 0,
				y: 0
			};

			this.init();
		};

		PIXI.IsometricMap.prototype = Object.create(PIXI.Container.prototype);
		PIXI.IsometricMap.prototype.constructor = PIXI.IsometricMap;
		/**
		 * init
		 *
		 * Initialization of the Isometric map. Set and configure listeners of events and so.
		 * @return {[type]} [description]
		 */
		PIXI.IsometricMap.prototype.init = function(){
			// Set Map handling parameters
			this._createCells();
			this._createContentCells();

			this.anchor = { x: 0.5, y: 0.5 };
			this.interactive = true;
			this.buttonMode = true;
			this.mousedown = this.touchstart = this._onMapPress;
			this.mouseup = this.touchend = this.mouseupoutside = this.touchendoutside = this._onMapUnpress;
			this.mousemove = this.touchmove = this._onMapMove;

			// Add Layers to map container
			this.addChild( this.layers.background );
			this.addChild( this.layers.parent );
			this.addChild( this.layers.ground );
			this.addChild( this.layers.behind );
			this.addChild( this.layers.decorations );
			this.addChild( this.layers.front );
			this.addChild( this.layers.grid );

			// Selector Box
			this._selectorBox = new PIXI.Graphics();
			this._selectorBox.lineStyle( 2, 0XFFFFFF );
		    this._selectorBox.moveTo( 0, this._tileSize / 2 );
		    this._selectorBox.lineTo( this._tileSize, 0 );
		    this._selectorBox.lineTo( 2 * this._tileSize, this._tileSize / 2 );
		    this._selectorBox.lineTo( this._tileSize, this._tileSize );
		    this._selectorBox.lineTo( 0, this._tileSize / 2 );
		    this._selectorBox.endFill();
			this._selectorBox.i = 0;
			this._selectorBox.j = 0;
			this.addChild( this._selectorBox );

			this.scaleMap( this._mapScale );
			this.centerMap();
			this._ticker.add( this._updateByTime.bind( this ) );
		};
		/**
		 * _createCells
		 *
		 * Creates all the cells of the isometric map. Is the base of all the view,
		 * over it all the elements are painted.
		 */
		PIXI.IsometricMap.prototype._createCells = function(){
			var size = this.n*2;
			if( !this.debugMode ){
				this._tileGap = 0;
			}
			for( var i = -this.n; i <= this.n; i++ ) {
			    for (var j = -this.n; j <= this.n; j++) {
			        this._initTile(i, j);
			        this._setTile(i, j);
			    }
			}
			this._minDrawPosition.x = this.width;
			this._minDrawPosition.y = this.height;
		};
		/**
		 * _createContentCells
		 * 
		 */
		PIXI.IsometricMap.prototype._createContentCells = function(){
			var size = this.n*2 + 1;
			for( var i = -this.n; i <= this.n; i++ ) {
				this.cellsContent[ i + this.n ] = new Array( size );
			    for (var j = -this.n; j <= this.n; j++) {
			    	this.cellsContent[i+this.n][j+this.n] = [];
			    }
			}
		};

		/**
		 * _setGraphicTileColor
		 *
		 * Set the color of a isometric cell if the map
		 * 
		 * @param {Array(Number)} idxs    [Indexes coordinates of the isometric cell]
		 * @param {HEX Number} color [ Color to fill the cell]
		 */
		PIXI.IsometricMap.prototype._setGraphicTileColor = function(idxs, color) {
		    var i = idxs[0];
		    var j = idxs[1];
		    var num = (i + this.n) * (2 * this.n + 1) + j + this.n;

		    if( num < this.layers.grid.children.length ){
			    var tile = this.layers.grid.getChildAt(num);
			    tile.clear()

			    var color = !_.isUndefined(color) ? color : null;
			    
			    if( !color ){
			    	if( i === 0 && j === 0 ){
			    		color = this.bgCenterColor;
				    }else if( ( Math.abs(i) + Math.abs(j) ) <= this.n ){
				    	color = this.bgVisibleColor;
				    }else{
				    	color = this.bgColor;
				    }
			    }
			    tile.color = color;
			    tile.beginFill( tile.color, 0.75 );
			    tile.lineStyle( 1, tile.color );
			    	tile.moveTo(tile.c1[0], tile.c1[1]);
			    	tile.lineTo(tile.c2[0], tile.c2[1]);
			    	tile.lineTo(tile.c3[0], tile.c3[1]);
			    	tile.lineTo(tile.c4[0], tile.c4[1]);
			    tile.endFill();
			}
		};
		/**
		 * _initTile
		 *
		 * Create a tile isometric square of the map for a single coordinate index.
		 * 
		 * @param  {[type]} i [description]
		 * @param  {[type]} j [description]
		 * @return {[type]}   [description]
		 */
		PIXI.IsometricMap.prototype._initTile = function( idxX, idxY ) {
		    var tile = new PIXI.Graphics();
		    tile.i = idxX;
		    tile.j = idxY;
		    tile.color = 0;
		    tile.c1 = this.indexToIso( idxX + 1 - this._tileGap, idxY + this._tileGap );
		    tile.c2 = this.indexToIso( idxX + 1 - this._tileGap, idxY + 1 - this._tileGap );
		    tile.c3 = this.indexToIso( idxX + this._tileGap, idxY + 1 - this._tileGap );
		    tile.c4 = this.indexToIso( idxX + this._tileGap, idxY + this._tileGap );
		    tile.hitArea = new PIXI.Polygon( [
		    	new PIXI.Point( tile.c1[ 0 ], tile.c1[ 1 ] ),
		    	new PIXI.Point( tile.c2[ 0 ], tile.c2[ 1 ] ),
		    	new PIXI.Point( tile.c3[ 0 ], tile.c3[ 1 ] ),
		    	new PIXI.Point( tile.c4[ 0 ], tile.c4[ 1 ] ) ]
		    );

		    this.layers.grid.addChild( tile );
		};
		/**
		 * _setTile
		 *
		 * Set the color and properties of a tile given by the index coordinates
		 * 
		 * @param {[type]} i     [description]
		 * @param {[type]} j     [description]
		 * @param {[type]} color [description]
		 */
		PIXI.IsometricMap.prototype._setTile = function(i, j) {

		    var num = (i + this.n) * (2 * this.n + 1) + j + this.n;
		    var tile = this.layers.grid.getChildAt(num);
		    tile.c1 = this.indexToIso(i + 1 - this._tileGap, j + this._tileGap);
		    tile.c2 = this.indexToIso(i + 1 - this._tileGap, j + 1 - this._tileGap);
		    tile.c3 = this.indexToIso(i + this._tileGap, j + 1 - this._tileGap);
		    tile.c4 = this.indexToIso(i + this._tileGap, j + this._tileGap);

		    this._setGraphicTileColor([i, j]);
		};
		/**
		 * indexToIso
		 *
		 * Calculates the coordinates in pixels inside the map by a grid indexes given
		 * 
		 * @param  {Number} idxX [Isometric grid index of coordinates to get]
		 * @param  {Number} idxY [Isometric grid index of coordinates to get]
		 * @return {Array(Number)}   [Corrdinates in pixels of the grid tile top vertex]
		 */
		PIXI.IsometricMap.prototype.indexToIso = function(idxX, idxY, scale) {
			var scale = scale || this._mapScale;
		    var x = this.offsetX + ( idxX - idxY * this._rotation ) * this._tileSize * scale;
		    var y = this.offsetY + ( idxY + idxX * this._rotation ) * this._tileSize * scale / this._isoCoef;
		    return [x, y];
		};
		/**
		 * isoToIndex
		 *
		 * Calculates the indexes of the map grid by a pixels coordinates given
		 * 
		 * @param  {Number} x [Pixel coordinate]
		 * @param  {Number} y [Pixel coordinate]
		 * @return {Array(Number)}   [Index coordinates calculated]
		 */
		PIXI.IsometricMap.prototype.isoToIndex = function(x, y, scale) {	
			var scale = scale || this._mapScale;	    
		    var s = x - this.offsetX * scale;
		    var t = y - this.offsetY * scale;

		    var idxY = ( t - s * this._rotation / this._isoCoef ) / ( 1 + this._rotation * this._rotation ) * this._isoCoef / ( this._tileSize * scale );
		    var idxX = ( s + idxY * this._tileSize * this._rotation * scale ) / ( this._tileSize * scale );

		    idxX = Math.floor(idxX);
		    idxY = Math.floor(idxY);

		    return [idxX, idxY];
		};
		/**
		 * _onMapPress
		 *
		 * Listener for the press event on the map
		 * 
		 * @param  {Object} e [Event object]
		 */
		PIXI.IsometricMap.prototype._onMapPress = function(e){	
			if( PIXI.isCanvasEvent( e ) ){
				this._mapDragging = true;
				this._dragOrigin.x = e.data.originalEvent.clientX;
				this._dragOrigin.y = e.data.originalEvent.clientY;
				var sx = e.data.getLocalPosition(this).x * this._mapScale;
				var sy = e.data.getLocalPosition(this).y * this._mapScale;
				var idx = this.isoToIndex(sx, sy);
				this._setGraphicTileColor( idx, '0x' + 'FFFFFF');
				//this.moveSelector( idx[0], idx[1] );
			}
		};
		/**
		 * _onMapUnpress
		 *
		 * Listener for the press finish event on the map
		 * 
		 * @param  {Object} e [Event object]
		 */
		PIXI.IsometricMap.prototype._onMapUnpress = function(e){

			this._mapDragging = false; 
			var sx = e.data.getLocalPosition(this).x * this._mapScale;
			var sy = e.data.getLocalPosition(this).y * this._mapScale;
			var idxCoords = this.isoToIndex(sx, sy);
			this._setGraphicTileColor(idxCoords);
			if( !this._vel.x && !this._vel.y && PIXI.isCanvasEvent( e ) ){
				
				var isoCoords = this.indexToIso( idxCoords[ 0 ], idxCoords[ 1 ] );
				this.onMouseUp( idxCoords );
			}
			this._vel.x = 0;
			this._vel.y = 0;
			
		};
		/**
		 * _onMapMove
		 *
		 * Listener for the move event on the map
		 * 
		 * @param  {Object} e [Event object]
		 */
		PIXI.IsometricMap.prototype._onMapMove = function(e){

			if (this._mapDragging) {

				var newPosition = {
			    	x: e.data.originalEvent.clientX,
			    	y: e.data.originalEvent.clientY
			    }
			    var deltaX = newPosition.x - this._dragOrigin.x;
			    var deltaY = newPosition.y - this._dragOrigin.y;
			    this.x += deltaX;
			    this.y += deltaY;
			    this._dragOrigin.x = newPosition.x;
				this._dragOrigin.y = newPosition.y;

				this._vel.x = deltaX;
				this._vel.y = deltaY;

				if( this.x > this._minDisp.x){ 
					this.x = this._minDisp.x;
				}
				if( this.x < this._maxDisp.x ){ 
					this.x = this._maxDisp.x;
				}
				if( this.y > this._minDisp.y){ 
					this.y = this._minDisp.y;
				}
				if( this.y < this._maxDisp.y ){ 
					this.y = this._maxDisp.y;
				}	
			}

			var sx = e.data.getLocalPosition(this).x * this._mapScale;
			var sy = e.data.getLocalPosition(this).y * this._mapScale;
			this.onMove( this.isoToIndex(sx, sy) ); 
		};
		
		/**
		 * centerMap
		 *
		 * Set the map to center position
		 */
		PIXI.IsometricMap.prototype.centerMap = function(){
			this.x = this._viewCenter.x;
			this.y = this._viewCenter.y;
		};
		/**
		 * scaleMap
		 *
		 * Calculates the parameters needed and set a new scale into the map
		 * 
		 * @param  {Number} scale [New scale to set]
		 */
		PIXI.IsometricMap.prototype.scaleMap = function( scale ){
			if( !_.isNaN( scale ) ){
				var deltaScale = scale / this._mapScale;
				var toCenterX = this.x - this._viewCenter.x;
				var toCenterY = this.y - this._viewCenter.y;

				this._mapScale = scale;
				this.scale.x = this._mapScale;
				this.scale.y = this._mapScale;

				//this._tileSize *= deltaScale;
				//var numLineCells = (this.n * 2 + 1);
				//this.offsetX = numLineCells * this._tileSize * this._mapScale;
				//this.offsetY = ( numLineCells * this._tileSize - this._tileSize ) * this._mapScale / this._isoCoef;

				//this._selectorBox.clear();
				

				this.moveSelector( this._selectorBox.i, this._selectorBox.j );

				/*var cellHeight =  this.indexToIso( 1, 1 )[1] - this.indexToIso( 0, 0 )[1];
				var cellWidth = this.indexToIso( 1, -1 )[0] - this.indexToIso( 0, 0 )[0];*/

				var topLeftCoords = ( this.debugMode ) ? [ 0, 0 ] : this.indexToIso( -1 * this.n, 0 );
				var bottomRightCoords = ( this.debugMode ) ? [ this.width, this.height ] : this.indexToIso( this.n, 0 );
				var mapWidth = bottomRightCoords[0] - topLeftCoords[0];
				var mapHeight = bottomRightCoords[1] - topLeftCoords[1];
				var deltaX = Math.round( ( this.viewport.width - mapWidth ) / 2 );
				var deltaY = Math.round( ( this.viewport.height - mapHeight ) / 2 );
				this._viewCenter.x = ( -1 ) * topLeftCoords[0] + deltaX;
				this._viewCenter.y = ( -1 ) * topLeftCoords[1] + deltaY;
				this._minDisp.x = ( deltaX >= 0 ) ? this._viewCenter.x : ( -1 ) * topLeftCoords[0];
				this._minDisp.y = ( deltaY >= 0 ) ? this._viewCenter.y : ( -1 ) * topLeftCoords[1];
				this._maxDisp.x = ( deltaX >= 0 ) ? this._viewCenter.x : this._minDisp.x + 2*deltaX;
				this._maxDisp.y = ( deltaY >= 0 ) ? this._viewCenter.y : this._minDisp.y + 2*deltaY;

				// Current Position
				
				this.x = this._viewCenter.x  + toCenterX * deltaScale;
				this.y = this._viewCenter.y + toCenterY * deltaScale;
			}
		};

		PIXI.IsometricMap.prototype.moveSelector = function( i, j ){
			var iso = this.indexToIso( i, j, 1 );
			this._selectorBox.i = i;
			this._selectorBox.j = j;
			this._selectorBox.position.x = iso[0] - this._tileSize;
			this._selectorBox.position.y = iso[1];
		};

		PIXI.IsometricMap.prototype.setBeingIntoGrid = function( being ){
			if(being){
				var gridIndex = this.isoToIndex( being.position.x, being.position.y, 1 );
				gridIndex[0] += this.n;
				gridIndex[1] += this.n;

				if( _.isArray( being.gridIndex ) ){
					this.removeBeingFromGrid( being );
				}
				being.gridIndex = gridIndex;
				this.cellsContent[ gridIndex[0] ][ gridIndex[1] ].push( being.id );
			}
		};

		PIXI.IsometricMap.prototype.getBeingById = function( Id ){
			return this._beingList[Id];
		};

		PIXI.IsometricMap.prototype.getBeingsByCell = function( cellIdx ){
			var response = [];
			if( _.isArray( cellIdx ) ){
				var beingsInCell = this.cellsContent[cellIdx[0]][cellIdx[1]];
				if(!beingsInCell){
					window.console.error('Being Cell Idx doen´t exist');
					window.console.log(cellIdx);
				}else{
					response = this.cellsContent[cellIdx[0]][cellIdx[1]].slice();
				}
			}
			return response;
		};
		
		PIXI.IsometricMap.prototype.removeBeingFromGrid = function(being){
			if(being){
				var cellContent = this.cellsContent[being.gridIndex[0]][being.gridIndex[1]];
				var posInCell = cellContent.indexOf(being.id);
				if( posInCell !== -1){
					cellContent.splice(posInCell,1);
				}else{
					window.console.warn('IsometricMap.setBeingIntoGrid: Should be an Element id '+being.id+' in the cell '+being.gridIndex[0]+','+being.gridIndex[1]);
				}
			}
		};

		PIXI.IsometricMap.prototype._updateByTime = function( deltaTime ){
			this.updateByTime( this._ticker.elapsedMS );
			for ( var bId in  this._beingList ) {
				this._beingList[ bId ].update(this._ticker.elapsedMS );
				this.setBeingIntoGrid( this._beingList[ bId ] );
			}
		};
		PIXI.IsometricMap.prototype.updateByTime = function(elapsedMS){};

		PIXI.IsometricMap.prototype.addElement = function( elementID, idxX, idxY, elDef, parent ){
			if( elDef ){
					// RENDERER DEFINITION
						// Default definition
						var definition = elDef; 
						
					// Get the cell indexes
						var coords = this.indexToIso( Number( idxX ), Number( idxY ), 1 );
					// LAYER
						var layer = layer || definition.layer || 'ground';
					// TYPE
						var element = null;

						if( definition.type === 'PLACEHOLDER' ){
							var src = definition.src[ Math.round( Math.random() * ( definition.src.length - 1 ) ) ];
							element = new PIXI.Sprite.fromImage( this._path + src );
						}else if( definition.type === 'image' ){
							element = new PIXI.Sprite.fromImage( this._path + definition.src );
						}else if( definition.type === 'light' ){
							element = new PIXI.Light2D( new PIXI.Texture.fromImage( this._path + definition.src ), this._renderer );
						}else if( definition.texture ){
							element = new PIXI.Sprite( definition.texture );
						}else if(  definition.group ){
							element = definition.group;
						}else if(  definition.type ===  'movieClip' ){
							var frames = [];
							for (var i = 0; i < definition.src.length; i++) {
								frames.push( this._path + definition.src[ i ] );
							}
							element = new PIXI.extras.MovieClip.fromImages( frames );
							element.animationSpeed = definition.animationSpeed || 1;
							element.play();
						}

					if( element ){
						element.position.x = coords[ 0 ] + ( Number( definition.x ) || 0 );
						element.position.y = coords[ 1 ] + ( Number( definition.y ) || 0 );
						element.scale.x = ( definition.scale ) ? Number( definition.scale.x ) : 1;
						element.scale.y = ( definition.scale ) ? Number( definition.scale.y ) : 1;
						element.rotation = Number( definition.rotation ) || 0;
						if( definition.blendMode && !_.isUndefined( PIXI.BLEND_MODES[ definition.blendMode ] ) ){
							element.blendMode =  PIXI.BLEND_MODES[ definition.blendMode ];
						}	
						if( !_.isUndefined( definition.alpha ) ){
							element.alpha = Number( definition.alpha );
						}	

						if( element.position.x < this._minDrawPosition.x )			{
							this._minDrawPosition.x = element.position.x;
						}
						if( element.position.y < this._minDrawPosition.y )			{
							this._minDrawPosition.y = element.position.y;
						}

						element.id = elementID;
						element.layer = this.layers[ layer ];
						this.layers[ layer ].addChild( element );
						this.cellsContent[ idxX + this.n ][ idxY + this.n ].push( element );
					}
			}
		};

		PIXI.IsometricMap.prototype.deleteElement = function( elementID, idxX, idxY ){
			var elements = this._findElementByIndex( elementID, idxX, idxY, true );
			for (var i = 0; i < elements.length; i++) {
				elements[i].layer.removeChild( elements[i] );
			}
		};

		PIXI.IsometricMap.prototype.deleteCellContent = function( idxX, idxY ){
			var cellContent = this.cellsContent[ idxX + this.n ][ idxY + this.n ];
			for (var i = cellContent.length - 1; i >= 0; i--) {
				cellContent[i].layer.removeChild( cellContent[i] );
			}
			cellContent = [];
		};

		PIXI.IsometricMap.prototype._findElementByIndex = function( elementID, idxX, idxY, remove ){
			var cellContent = this.cellsContent[ idxX + this.n ][ idxY + this.n ];
			var elements = [];
			// Find By Cell
			for (var i = cellContent.length - 1; i >= 0; i--) {
				if( cellContent[i].id === elementID ){
					elements.push( cellContent[i] );
					if( remove ){
						cellContent.splice( i, 1 );
					}
				}
			}
			return elements;
		}

		PIXI.IsometricMap.prototype.play = function(){

			for( var layerName in this.layers ){

				for (var c = 0; c < this.layers[ layerName ].children.length; c++) {
					var child = this.layers[ layerName ].children[c];
					if( child.play ){
						child.play();
					}
				}
			}
		};

		PIXI.IsometricMap.prototype.stop = function(){
			for( var layerName in this.layers ){
				for (var c = 0; c < this.layers[ layerName ].children.length; c++) {
					var child = this.layers[ layerName ].children[c];
					if( child.gotoAndStop ){
						child.gotoAndStop( 0 )
					}
				}
			}
		};

		PIXI.IsometricMap.prototype.clear = function(){
			// Cleans Layers but 'grid' one
			for(var lName in this.layers){
				if( lName !== 'grid' ){
					var layer = this.layers[ lName];
					if( layer.cacheAsBitmap ){
						layer.cacheAsBitmap = false;
					}
					layer.removeChildren();
				}
			}
			
			// Cleans Beings
			for (var bId in  this._beingList) {
				this._beingList[bId].destroy();
			}

			// Cell Content
			this._createContentCells();

			// Callback
			if( this.onClear ){
				window.setTimeout(function(){ this.onClear(); }.bind(this), 10);
			}
		};
		

		PIXI.IsometricMap.prototype.addBeing = function( being ){
			if( being && being.id ){
				// Add to Stage
				this.layers.behind.addChildAt( being.sprite, 0 );
				// Add to Being List
				this._beingList[being.id.toString()] =  being;
				// Add to Grid
				this.setBeingIntoGrid(being);
			}
		};

		PIXI.IsometricMap.prototype.destroyBeing = function(id){
			var being = this._beingList[id];
			if(being){
				// Remove from Stage
				this.layers.behind.removeChild( being.sprite );
				// Remove from Grid
				this.removeBeingFromGrid(being);
				// Remove from Being List
				delete this._beingList[id];
			}else{
				window.console.warn("IsoMap.destroyBeing: No being founded with the id "+id);
			}
		};

		PIXI.IsometricMap.prototype.destroyAllBeings = function(){
			for (var bId in  this._beingList) {
				this.destroyBeing(bId);
			}
		};

		PIXI.IsometricMap.prototype.disable = function(){
			this.interactive = false;
			this.buttonMode = false;
		};

		PIXI.IsometricMap.prototype.enable = function(){
			this.interactive = true;
			this.buttonMode = true;
		};

		PIXI.IsometricMap.prototype.resize = function( width, height ){
			this.viewport = { 
				width: width || 100, 
				height: height || 100 
			};
			this.scaleMap( 1 );
			this.centerMap();
		};

		PIXI.IsometricMap.prototype.moveToParent = function(){
			var layers = [ "front", "decorations", "behind", "ground" ];
			for (var i = 0; i < layers.length; i++) {
				var layer = this.layers[ layers[i] ];
				for (var c = layer.children.length - 1; c >= 0; c--) {
					var el = layer.getChildAt(  c );
					this.layers.parent.addChildAt( el, 0 );
				}
				layer.removeChildren();
			}
		};

		PIXI.IsometricMap.prototype.set = function( i, j ){

		};

		PIXI.IsometricMap.prototype.onClear = null;
		PIXI.IsometricMap.prototype.onMove = function( cellsCoords ){};

		/**
		 * MapBeing
		 *
		 * Basic "life" element for a map. Could be anything and have all the basic
		 * behaviours to be handeled by a PIXI.IsometricMap.
		 * 
		 * @param {Object} isoMap 	[PIXI.IsometricMap where is going to live]
		 * @param {String} id     	[Id of this being to indetified any time by others]
		 * @param {Object} sprite 	[PIXI.Sprite to have a graphic expression]
		 * @param {Array} path   	[Route points as positions where this being is going to move in line]
		 * @param {Object} opt   	[Object with configuration options]
		 */
		PIXI.MapBeing = function(isoMap, id, sprite, path, opt){
			var opt = opt || {};
			this.type = opt.type || '';
			this.id = id;
			this.isoMap = isoMap;
			this.sprite = sprite;

			this.topSpeed = opt.topSpeed || 0.1;

			this.acel = {
				x:0, 
				y:0
			};
			this.vel = {
				x:0, 
				y:0
			};
			this.position = {
				x:0, 
				y:0
			};
			this.axis = ''; // 'x' or 'y'. Axis the element is moving on.
			this.way = ''; 	//  'plus' or 'less'. Way into the axis the element is moving on.


			this.path = path || [];
			this.pathIndex = 0;
			this.target = {
				position:{x:0,y:0}
			};

			this.init();
		};

		PIXI.MapBeing.prototype.init = function(){

			var initPos = this.path[0];
			var initCoords = this.isoMap.indexToIso( initPos[ 0 ], initPos[ 1 ] );
			this.position.x = initCoords[0];
			this.position.y = initCoords[1];

			this.setNewTarget();
		};

		PIXI.MapBeing.prototype.setNewTarget = function(){
			// Set Next Target
			var cIdx = this.path[this.pathIndex];
			this.pathIndex++;
			var tIdx = this.path[this.pathIndex];

			var aw = this.getAxisWay(cIdx, tIdx);
			this.onAxisWayChange(aw[0],aw[1]);

			var targetCoords = this.isoMap.indexToIso( tIdx[ 0 ], tIdx[ 1 ] );
			this.target.position = {
				x: targetCoords[0],
				y: targetCoords[1]
			}
		};

		PIXI.MapBeing.prototype.getAxisWay = function(pos1,pos2){
			var axis = 'x';
			var way = 'less';

			if(pos1[0] === pos2[0]){
				axis = 'y';
				if(pos1[1] < pos2[1]){
					way = 'plus';
				}
				if(pos1[1] > pos2[1]){
					way = 'less';
				}
			}
			if(pos1[1] === pos2[1]){
				axis = 'x';
				if(pos1[0] < pos2[0]){
					way = 'plus';
				}
				if(pos1[0] > pos2[0]){
					way = 'less';
				}
			}
			
			return [axis,way];
		};

		PIXI.MapBeing.prototype.onAxisWayChange = function(axis, way){

			if(axis === 'x'){
				this.sprite.scale.x = -1;
				//this.sprite.anchor.x = -1.5;
			}else{
				this.sprite.scale.x = 1;
				//this.sprite.anchor.x = 0;
			}

			this.way = way;
			this.axis = axis;

			this.vel.x = 0;
			this.vel.y = 0;
			this.vel.x = (this.axis === 'x' && this.way === 'plus' || this.axis === 'y' && this.way === 'less') ? this.topSpeed*2 : -this.topSpeed*2;
			this.vel.y = (this.way === 'plus') ? this.topSpeed : -this.topSpeed;
		};

		PIXI.MapBeing.prototype.onTarget = function(){	
			if(this.pathIndex < this.path.length-1){
				this.setNewTarget();
			}else{
				// Destroy the element
				this.destroy();
			}
		};

		PIXI.MapBeing.prototype.update = function(elapsedMS){
			this.position.x += this.vel.x * elapsedMS;
			this.position.y += this.vel.y * elapsedMS;

			if( 	this.way < 0  && this.position.y <= this.target.position.y 
				||	this.way > 0  && this.position.y >= this.target.position.y ){
				
				this.position.x = this.target.position.x;
				this.position.y = this.target.position.y;

				this.onTarget();
			}
			if(this.sprite){
				this.sprite.position.x = this.position.x;
				this.sprite.position.y = this.position.y;
			}
		};

		PIXI.MapBeing.prototype.destroy = function(){
			if(this.sprite.parent){
				this.sprite.parent.removeChild(this.sprite);
			}
			this.sprite.destroy();
			this.sprite = null;
			this.isoMap.destroyBeing(this.id);
		};

	PIXI.Light2D = function( texture, renderer ){

		PIXI.Sprite.call( this, texture );

		if( renderer instanceof PIXI.CanvasRenderer ){
			this.blendMode = PIXI.BLEND_MODES.OVERLAY;
		}else{
			this.blendMode = PIXI.BLEND_MODES.SCREEN;
			this.alpha = 0.25;
		}
	};

	PIXI.Light2D.prototype = Object.create( PIXI.Sprite.prototype );
	PIXI.Light2D.prototype.constructor = PIXI.Light2D;

	/**
	 * @override _renderWebGL
	 */
/*	PIXI.Light2D.prototype._renderWEbGL = ._renderWebGL = function ( renderer ){

	    this.calculateVertices();

	    var bg = renderer.view.toDataURL();
	    //this.vertexData();
	    renderer.setObjectRenderer(renderer.plugins.sprite);
	    renderer.plugins.sprite.render(this);
	};
*/

		return PIXI;
	}

);