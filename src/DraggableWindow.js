define( function() {

    	var DraggableWindow = function( DOMEl, x, y ){
            this._el = DOMEl;
            this._dragSuface = null;
            this._startPoint = [];
            this._position = {
                x: x || 0,
                y: y || 0
            };
            this._isDragging = false;

            this._setPosStyle();

            this._dragSuface = this._el.querySelector('.drag_surface');
            if( !this._dragSuface ){
                this._dragSuface = this._el;
            }
            this._dragSuface.addEventListener('mousedown', this._startDrag.bind( this ), false );
            this._dragSuface.addEventListener('mousemove', this._moveDrag.bind( this ), false );
            document.body.addEventListener('mouseup', this._endDrag.bind( this ), false );
        };

        DraggableWindow.prototype._startDrag = function( e ){
            this._isDragging = true;
            this._startPoint = [
                e.x || e.pageX || e.clientX,
                e.y || e.pageY || e.clientY
            ];
        };

        DraggableWindow.prototype._moveDrag = function( e ){
            if( this._isDragging ){
                var cPoint = [
                    e.x || e.pageX || e.clientX,
                    e.y || e.pageY || e.clientY
                ];
                this._position.x += cPoint[0] - this._startPoint[0];
                this._position.y += cPoint[1] - this._startPoint[1];
                this._setPosStyle();
                this._startPoint = cPoint;
            }
        };

        DraggableWindow.prototype._endDrag = function( e ){
            this._isDragging = false;
        };

        DraggableWindow.prototype._setPosStyle = function(){
            this._el.style.left = this._position.x.toString() + 'px';
            this._el.style.top = this._position.y.toString() + 'px';
        };

    	return DraggableWindow;
    }
);